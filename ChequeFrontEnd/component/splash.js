import React,{Component} from 'react';
import {Text,StyleSheet,View,ImageBackground,Image} from 'react-native';
import splashImage from '../Images/1176.jpg'
import overImage from '../Images/overImage.png'
import check from '../Images/cheque.png'
import {AsyncStorage} from 'react-native'
import {strings} from './Strings'


class Splash extends Component{
    state={
        userid:''
    }
    componentDidMount(){
        strings.setLanguage('ar')
        AsyncStorage.getItem('userId').then((userId)=>{
            if(userId){
                setTimeout(() => { this.props.navigation.navigate('App')}, 1500)
            }
            else{
                setTimeout(() => { this.props.navigation.navigate('Auth')}, 1500)
            }
        })



       
    }
render(){


return (
    <View style={styles.continer}>
        <ImageBackground  source={splashImage} style={styles.ImageBackGround}>
         <ImageBackground source={overImage} style={styles.ImageBackGround}> 
         
        </ImageBackground>
        </ImageBackground>
    </View>
)

}
}

const styles=StyleSheet.create({
continer:{
    flex:1,
},
ImageBackGround:{
    flex:1,
    justifyContent:'center',
    alignItems:'center',
    resizeMode: 'cover',
    alignSelf: 'stretch',
    flexDirection: 'column',
    marginTop:-30,
   
},
AppTitle:{
    fontSize:33,
    fontWeight:'bold',
    color:'#ffde03',
},
logoApp:{
  borderWidth:0,
  borderColor:'#ffde03'
}

})
export default Splash
