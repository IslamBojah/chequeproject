import React,{Component} from 'react';
import {StyleSheet,KeyboardAvoidingView,Picker,View,TextInput,Text,
    ScrollView,ActivityIndicator,Button,NetInfo} from 'react-native'

import {strings} from './Strings'
import HanderError from './inlineError'
import {connect} from 'react-redux'
import {CreateUser} from '../actions/actions'

class Signup extends Component{
    state={
        language:'en',
        usernameFilled:styles.InputFiled,
        phoneInputFiled:styles.InputFiled2,
        passwordInputFiled:styles.InputFiled,
        confirmPasswordFiled:styles.InputFiled,
        username:'',
        Mobile:'',
        Password:'',
        confirmPassword:'',
        Errors:{},
        backerror:'',
        loading:true,
        mobileDivFilled:styles.mobileDiv,
        introductionValue:'970',


    }
    static navigationOptions=({navigation})=>{
        return{
            title:strings.CreatedAccount,
            headerStyle:{backgroundColor:'rgb(148,20,103)'},
            headerTitleStyle:{color:'white'},
            headerTintColor:'white',
            headerRight:null

        }
    }
   
    onValueChange=(value, index)=>{
        strings.setLanguage(value)
        this.setState({language:value})   
      }
    handlerChnageLange=()=>{
        return(
          <Picker
          onStartShouldSetResponder={true}
          selectedValue={strings.getLanguage()}
          style={{height: 20, width: 200, color:"#fff",backgroundColor :'transparent'}}
          onValueChange={this.onValueChange}>
          <Picker.Item  label="Arabic" value="ar" />
          <Picker.Item label="English" value="en" />
          </Picker>
        )
    }
    Handler_signUp=()=>{
        const Errors=this.hander_Validation(this.state)
        this.setState({Errors},()=>{ 
            if(this.state.Errors.login==""){
                NetInfo.isConnected.fetch().done((isConnected)=>{
                    if(isConnected){

                        data={user_name:this.state.username , user_phone:this.state.introductionValue+this.state.Mobile,user_password:this.state.Password}
                        this.setState({loading:false})
                        this.props.SignUp(data).then(res=>{
                            this.setState({loading:true})
                            if(this.props.data.success==false){
                                this.setState({backerror:this.props.data.error})
                            }      
                            else{
                                this.setState({backerror:''})
                                this.props.navigation.navigate('Home')
                            }
                        })

                    }
                    else{
                        this.setState({loading:false})
                        setTimeout(function(){
                            this.setState({loading:true})
                            alert('no Internet connection')
                        }.bind(this),1000)
                    }
                })
        
            }
        }) 
    }

    userNameChange=(username)=>{
        this.setState({username},
            ()=>{
                if(!(/[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z ]+[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z-_ ]$/).test(this.state.username)){
                    this.setState({usernameFilled:styles.InputFiledError})
                    this.setState({
                        Errors:{...this.state.Errors,login:strings.userNameError}
                    })
                }
                else{
                    this.setState({usernameFilled:styles.InputFiled})
                    this.setState({
                        Errors:{...this.state.Errors,login:""}
                    })
                }

            })
    }
    MobileChange=(Mobile)=>{
        this.setState({Mobile},
            ()=>{
                if(!((/^([9]{1})([7]{1})([0,2]{1})([5]{1})([0,2,4,5,6,9]{1})([0-9]{7})*$/).test(this.state.introductionValue+this.state.Mobile))){
                    this.setState({mobileDivFilled:styles.mobileDivError})
                    this.setState({
                        Errors:{...this.state.Errors,login:strings.mobileError}
                    })
                }
                else{
                    this.setState({mobileDivFilled:styles.mobileDiv})
                    this.setState({
                        Errors:{...this.state.Errors,login:""}
                    })
                }

            })
    }
    PasswordChange=(Password)=>{
        this.setState({Password},
            ()=>{
                if(!(/(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$/).test(this.state.Password)){
                    this.setState({passwordInputFiled:styles.InputFiledError})
                    this.setState({
                        Errors:{...this.state.Errors,login:strings.passwordValidation}
                    })
                }
                else{
                    this.setState({passwordInputFiled:styles.InputFiled})
                    this.setState({
                        Errors:{...this.state.Errors,login:""}
                    })
                }

            })
    }
    ConfirmPasswordChange=(confirmPassword)=>{
        this.setState({confirmPassword},
            ()=>{
                if(!(/(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$/).test(this.state.confirmPassword) || this.state.Password!=this.state.confirmPassword){
                    this.setState({passwordInputFiled:styles.InputFiledError})
                    this.setState({confirmPasswordFiled:styles.InputFiledError})

                    this.setState({
                        Errors:{...this.state.Errors,login:strings.passwordNotMatching}
                    })
                }
                else{
                    this.setState({passwordInputFiled:styles.InputFiled})
                    this.setState({confirmPasswordFiled:styles.InputFiled})
                    this.setState({
                        Errors:{...this.state.Errors,login:""}
                    })
                }

            })
    }
    hander_Validation=()=>{
        const errors={}
        if (!this.state.Mobile || !this.state.Password ||!this.state.username || !this.state.confirmPassword){
            errors.login=strings.PasswordMobileError
            if(!this.state.Mobile){
                this.setState({mobileDivFilled:styles.mobileDivError});
            }
            else{
                this.setState({mobileDivFilled:styles.mobileDiv});
            }
            if(!this.state.Password){
                this.setState({passwordInputFiled:styles.InputFiledError});
            }
            else{
                this.setState({passwordInputFiled:styles.InputFiled}); 
            }
            if(!this.state.username){
                
            }
            if(!this.state.confirmPassword){
                this.setState({confirmPasswordFiled:styles.InputFiledError})
            }
            else{
                this.setState({confirmPasswordFiled:styles.InputFiled})
            }
            if(!this.state.username){
                this.setState({usernameFilled:styles.InputFiledError})
            }
            else{
                this.setState({usernameFilled:styles.InputFiled})
            }
        } 
        else if(!(/[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z ]+[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z-_ ]{3,20}$/).test(this.state.username)){
            errors.login=strings.userNameError
            this.setState({usernameFilled:styles.InputFiledError})
        }
        else if (!((/^([9]{1})([7]{1})([0,2]{1})([5]{1})([0,2,4,5,6,9]{1})([0-9]{7})*$/).test(this.state.introductionValue+this.state.Mobile)) && this.state.Password){
            errors.login=strings.mobileError
            this.setState({mobileDivFilled:styles.mobileDivError});
            this.setState({passwordInputFiled:styles.InputFiled});
        }
        else if (!(/(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$/).test(this.state.Password) && this.state.Mobile){
            errors.login=strings.passwordError
            this.setState({mobileDivFilled:styles.mobileDiv});
            this.setState({passwordInputFiled:styles.InputFiledError});
        }
        else if (!(/(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$/).test(this.state.confirmPassword) && this.state.Password!=this.state.confirmPassword){
            this.setState({passwordInputFiled:styles.InputFiledError});
            this.setState({confirmPasswordFiled:styles.InputFiledError})
            errors.login=strings.passwordNotMatching
        }
        else{
            errors.login=""
            this.setState({usernameFilled:styles.InputFiled})
            this.setState({passwordInputFiled:styles.InputFiled});
            this.setState({mobileDivFilled:styles.mobileDiv})
            this.setState({confirmPasswordFiled:styles.InputFiled})
        }
        return errors
    }

    _renderButton(){
        if(this.state.loading){
            return (
                <Button title={strings.SignUp} onPress={this.Handler_signUp} color='#941467'></Button> 
                );
            }
            else{
               return(
                <ActivityIndicator size='large' color="#007aff"/>
               ); 
            }
    }
    render(){
        return(
            <ScrollView contentContainerStyle={{flexGrow: 1}}>
            <KeyboardAvoidingView style={styles.contianer}> 
                <TextInput 
                    style={this.state.usernameFilled}
                    placeholder={strings.username}
                    returnKeyType="next"
                    onChangeText={this.userNameChange}
                    
                 />

                <View style={this.state.mobileDivFilled}>

                    <Picker style={{width:105}}
                            onStartShouldSetResponder={true}
                            selectedValue={this.state.introductionValue}
                            onValueChange={(item)=>this.setState({introductionValue:item})}>
                        <Picker.Item label='+970'  value="970"></Picker.Item>
                        <Picker.Item label='+972'  value="972"></Picker.Item>
                    </Picker>

                    <TextInput 
                    style={this.state.phoneInputFiled}
                    placeholder={strings.mobleNumber}
                    returnKeyType="next"
                    onChangeText={this.MobileChange}
                    keyboardType="phone-pad"
                    maxLength={9}
                    />

                </View>

                  <TextInput 
                    style={this.state.passwordInputFiled}
                    placeholder={strings.password}
                    returnKeyType="next"
                    onChangeText={this.PasswordChange}
                    secureTextEntry={true}
                 />
                  <TextInput 
                    style={this.state.confirmPasswordFiled}
                    placeholder={strings.confirmPassword}
                    returnKeyType="next"
                    onChangeText={this.ConfirmPasswordChange}
                    secureTextEntry={true}
                 />
                <View><HanderError>{this.state.Errors.login}</HanderError></View>
                <View><HanderError>{this.state.backerror}</HanderError></View>
               
                <View style={{marginTop:10,width:300}}>
                 {this._renderButton()}
                
                </View>
              
               
            </KeyboardAvoidingView>
            </ScrollView>
            
            
        );
    }
}

const styles=StyleSheet.create({
    Home:{
        flex:1
    },
    contianer:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        marginTop:10,
        
       
      
    },
    divHome:{
        flex:1,
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'center',
     
    },  
     InputFiled:{
        marginTop:10,
        marginLeft:30,
        marginRight:30,
        borderColor:'#c0c0c0',
        borderWidth:1,
        borderRadius:5,
        backgroundColor:'#fff',
        alignSelf:'stretch',
        shadowColor:'#000',
    },
    InputFiled2:{
        marginLeft:5,
        marginRight:5,
        borderColor:'#c0c0c0',
        backgroundColor:'#fff',
        alignSelf:'stretch',
        shadowColor:'#000', 
        flex:1 , 
        
    },
    InputFiledError:{
        marginTop:10,
        marginLeft:30,
        marginRight:30,
        borderColor:'#d50000',
        borderWidth:1,
        borderRadius:5,
        backgroundColor:'#fff',
        alignSelf:'stretch',
        shadowColor:'#000',
    },
    ElementDiv:{
        marginTop:10,
        marginBottom:10,
       
        
    },  
      TextStyle:{
        alignSelf:'center',
        color:"#007aff",
        fontSize:20,
        fontWeight:'600',
        paddingTop:10,
        paddingBottom:10,

    },
    mobileDiv:{
        height:50,
        marginTop:10,
        marginLeft:30,
        marginRight:30,
        borderColor:'#c0c0c0',
        borderWidth:1,
        borderRadius:5,
        backgroundColor:'#fff',
        alignSelf:'stretch',
        shadowColor:'#000',
        flexDirection:'row',
    },
    mobileDivError:{
        height:50,
        marginTop:10,
        marginLeft:30,
        marginRight:30,
        borderColor:'#d50000',
        borderWidth:1,
        borderRadius:5,
        backgroundColor:'#fff',
        alignSelf:'stretch',
        shadowColor:'#000',
        flexDirection:'row',
    },
})


const mapToProps = dispatch =>{
    return{
      SignUp:(data)=>dispatch(CreateUser(data))
    }

  }

const mapToState =state =>{
    return{
      data:state.auth
    }
  }
export default connect(mapToState,mapToProps)(Signup)