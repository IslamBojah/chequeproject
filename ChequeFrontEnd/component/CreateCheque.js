import React,{Component} from 'react'
import {View,StyleSheet,Text,TouchableOpacity,KeyboardAvoidingView,Picker,TextInput,
    ScrollView,Image,ActivityIndicator,Alert,FlatList} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import Modal from 'react-native-modal'
import {strings} from './Strings'
import {connect} from 'react-redux'
import {CreateNewBook,GetAllBooksFun,EditBookFun,DeleteBookFun} from '../actions/actions'
import {AsyncStorage} from 'react-native'
import icon from '../Images/checkIcon.png'
import arrow from '../Images/arrow.png'
import arrow2 from '../Images/backArabic.png'
import DialogInput from 'react-native-dialog-input'
import { NavigationEvents } from "react-navigation";


class CreateCheque extends Component{
    state = {
        isModalVisible: false,
        coins:[strings.ILS , strings.JD , strings.USD ],
        numberofPaper:['10','15','20','30','Other'],
        coin:strings.ILS,
        BookName:'',
        banks:strings.banks,
        bank:'',
        paperNum:'',
        user_id:'',
        userNameStyle:styles.InputFiled,
        bankStyle:styles.InputFiled,
        coinStyle:styles.InputFiled,
        paperNumStyle:styles.InputFiled,
        loading:false,

        bookData:'',
        book_id:'',
        EditingData:'',
        EditModelVisible:false,


        otherBankDialog:false,
        otherPageNumberDialoge:false,


        

      }
  
    
      handerChnageBank=(value)=>{
        if(value=='Other'){
            this.setState({otherBankDialog:true})
        }
        else{
            this.setState({...this.state.bank,
                bank:value
            },()=>{
                if (this.state.bank !=""){
                    this.setState({bankStyle:styles.InputFiled})
                }
                else{
                    this.setState({bankStyle:styles.InputFiledError})
                }
            })
        }
    }

    dismissOtherDialoge=(value)=>{
        if(value != ''){
            this.setState({bank:value},()=>{
                this.setState({otherBankDialog:false})
                this.handerChnageBank(value)
                this.state.banks.unshift(value)
            })
        }
        
    }

    handerChnagepageNumber=(value)=>{
        if(value=='Other'){
            this.setState({otherPageNumberDialoge:true})
        }
        else{
            this.setState({...this.state.paperNum,
                paperNum:value
            },()=>{
                if (this.state.paperNum !=""){
                    this.setState({paperNumStyle:styles.InputFiled})
                }
                else{
                    this.setState({paperNumStyle:styles.InputFiledError})
                }
            })
        }
    }

    dismissOtherPageNumberDialoge=(value)=>{
        if(value != ''){
            this.setState({paperNum:value},()=>{
                this.setState({otherPageNumberDialoge:false})
                this.handerChnagepageNumber(value)
                this.state.numberofPaper.unshift(value)
            })
        }
        
    }


    componentDidMount(){
        AsyncStorage.getItem('userId').then(user_id=>{
            this.props.GetAllBooksFun({user_id:user_id}).then(
                res=> this.setState({bookData:Array.prototype.reverse.call(this.props.data.data)})
             )
             this.setState({user_id})
        })
   
   
    }

    

    _toggleModal = () =>{
        this.setState({ isModalVisible: !this.state.isModalVisible });
        this.setState({BookName:''})
        this.setState({coin:strings.ILS})
        this.setState({bank:''})
        this.setState({paperNum:''})
        this.setState({userNameStyle:styles.InputFiled})
        this.setState({bankStyle:styles.InputFiled})
        this.setState({coinStyle:styles.InputFiled})
        this.setState({paperNumStyle:styles.InputFiled})
    }


    _CreateNewBook =() =>{
        error1=false
        error2=false
        error3=false
        error4=false
        if(this.state.BookName == '' || !(/[0-9\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z ]+[0-9\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z-_ ]{1,}$/).test(this.state.BookName) ){
            this.setState({userNameStyle:styles.InputFiledError})
            error1=true
        }
        else{
            this.setState({userNameStyle:styles.InputFiled})
            error1=false
        }
        if(this.state.coin ==''){
            this.setState({coinStyle:styles.InputFiledError})
            error2=true
        }
        else{
            this.setState({coinStyle:styles.InputFiled})
            error2=false
        }
        if(this.state.bank ==''){
            this.setState({bankStyle:styles.InputFiledError})
            error3=true
        }
        else{
            this.setState({bankStyle:styles.InputFiled})
            error3=false
        }
        if(this.state.paperNum==''){
            this.setState({paperNumStyle:styles.InputFiledError})
            error4=true
        }
        else{
            this.setState({paperNumStyle:styles.InputFiled})
            error4=false
        }

        if(error1==false && error2==false &&error3==false &&error4==false ){
            
            const today=new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();

            var date =yyyy+'-'+mm+'-'+dd

            const obj={book_name:this.state.BookName , coin_name:this.state.coin , bank_name:this.state.bank , book_size:this.state.paperNum , status:1 ,user_id:this.state.user_id ,date:date }


            this.props.CreateNewBook(obj).then(res=>{
                this.state.bookData.unshift(this.props.data.data[0])
            })
            this.setState({loading:true})

            setTimeout(function(){
                this.setState({loading:false})
                this.setState({ isModalVisible: !this.state.isModalVisible });
            }.bind(this),1000)


            this.setState({BookName:''})
            this.setState({coin:strings.ILS})
            this.setState({bank:''})
            this.setState({paperNum:''})
            this.setState({userNameStyle:styles.InputFiled})
            this.setState({bankStyle:styles.InputFiled})
            this.setState({coinStyle:styles.InputFiled})
            this.setState({paperNumStyle:styles.InputFiled})

        }
    }
    _onPress =(rowData)=>{
//        alert('Hello World')
        this.props.navigation.navigate('BookCheques',{book_id:rowData.book_id ,currency:rowData.coin_name ,bankName:rowData.bank_name ,bookSize:rowData.book_size , available_size:rowData.available_size})
    }
    _onLongPress=(rowData)=>{    
       
       Alert.alert(
        strings.EditBook,
        'My Alert Msg',
        [
         {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
         },
          {text:strings.EditItem, onPress: () => {
            this.setState({EditModelVisible:true})
            this.setState({EditingData:rowData}) 
            this.setState({BookName:rowData.book_name})
            this.setState({coin:rowData.coin_name})
            this.setState({bank:rowData.bank_name})
            this.setState({paperNum:rowData.book_size})
            this.setState({book_id:rowData.book_id})
          }
        },
        {text:strings.DeleteItem, onPress: () => Alert.alert(
            strings.ConfirmDeleteItem,
            strings.DeleteItem,
            [
                {
                    text:strings.ok ,onPress:()=>{
                        this.props.DeleteBookFun({book_id:rowData.book_id})
                        const newBookData = this.state.bookData.filter((user)=>{
                            return user.book_id != rowData.book_id
                        })
 
                        this.setState({bookData:newBookData})
                    }
                },
                {
                   text:strings.cancel 
                }
            ]
          )
        },
        ],
        {cancelable: true},
       )       
    }
    _EditRowToggle=()=>{
        this.setState({EditModelVisible:false})
    }
    _EditBookFun=()=>{
        error1=false
        error2=false
        error3=false
        error4=false
        if(this.state.BookName == '' || !(/[0-9\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z ]+[0-9\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z-_ ]{1,}$/).test(this.state.BookName) ){
            this.setState({userNameStyle:styles.InputFiledError})
            error1=true
        }
        else{
            this.setState({userNameStyle:styles.InputFiled})
            error1=false
        }
        if(this.state.coin ==''){
            this.setState({coinStyle:styles.InputFiledError})
            error2=true
        }
        else{
            this.setState({coinStyle:styles.InputFiled})
            error2=false
        }
        if(this.state.bank ==''){
            this.setState({bankStyle:styles.InputFiledError})
            error3=true
        }
        else{
            this.setState({bankStyle:styles.InputFiled})
            error3=false
        }
        if(this.state.paperNum==''){
            this.setState({paperNumStyle:styles.InputFiledError})
            error4=true
        }
        else{
            this.setState({paperNumStyle:styles.InputFiled})
            error4=false
        }

        if(error1==false && error2==false &&error3==false &&error4==false ){
            {
        const obj={book_name:this.state.BookName ,book_id:this.state.book_id ,coin_name:this.state.coin , bank_name:this.state.bank , book_size:this.state.paperNum , status:1 ,user_id:this.state.user_id }
       
        this.props.EditBookFun(obj)

        this.state.bookData.map((user)=>{
            if(user.book_id===this.state.book_id){

                user.book_name=this.state.BookName,
                user.book_id=this.state.book_id 
                user.coin_name=this.state.coin 
                user.bank_name=this.state.bank 
                user.available_size=parseInt(this.state.paperNum )-(parseInt(user.book_size)-parseInt(user.available_size))
                user.book_size=this.state.paperNum 
                user.status=1 
                user.user_id=this.state.user_id
            }
        })
        this.setState({EditModelVisible:false})
    
    }
}
    }
    onChangeBookName=(bookName)=>{
        this.setState({BookName:bookName},
            ()=>{
                if(!(/[0-9\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z ]+[0-9\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z-_ ]{1,}$/).test(this.state.BookName)){
                    this.setState({userNameStyle:styles.InputFiledError})
            }
            else{
                this.setState({userNameStyle:styles.InputFiled})
            }

        })

    }


render(){

return(
    <View style={styles.contianer}>
        
         <NavigationEvents
          onWillFocus={payload => {
            AsyncStorage.getItem('userId').then(user_id=>{
                this.props.GetAllBooksFun({user_id:user_id}).then(
                    res=> this.setState({bookData:Array.prototype.reverse.call(this.props.data.data)})
                 )
                 this.setState({user_id})
            })
        }}
        />
       <View style={styles.homeDiv}>
       <ScrollView style={{flex:1,width:'100%',marginBottom:5}}>
       {(this.state.bookData !=='' && Object.keys(this.state.bookData).length>0)   &&
         <FlatList
         data={this.state.bookData}
         extraData={this.state}
         renderItem={({item:rowData})=>{
             return(
                <TouchableOpacity style={styles.card} onPress={this._onPress.bind(this,rowData)} onLongPress={this._onLongPress.bind(this,rowData)}>

                <View style={styles.ImageCard}>
                    <Image source={icon} style={{width:30 , height:30 }}></Image>
                </View>

                <View style={styles.BodyCard}>
                    <View style={{flex:4 ,flexDirection:'column',marginTop:5}}>
                        <Text  style={{fontSize:17,fontWeight:'600',fontFamily:'bold',}} >{rowData.book_name}</Text>
                        <Text numberOfLines={1}>{rowData.bank_name}</Text>
                    </View>
                    <View style={{flex:1 , marginTop:10}}>
                    <Text  style={{color:rowData.available_size=='0'?'#d50000':'green'}} >{rowData.available_size}</Text>
                    </View>
                    <View style={{flex:2,flexDirection:'column',margin:5}}>
                        <Text  style={{alignSelf:'flex-end',color:'#d50000'}} >{rowData.coin_name}</Text>
                        <Text  style={{alignSelf:'flex-end',fontStyle:'italic', textAlignVertical:'center'}} >{rowData.date}</Text>
                    </View>
                </View>

                <TouchableOpacity style={styles.deleteCard} onPress={this._onPress.bind(this,rowData)}>
                    <Image source={strings.getLanguage()=='ar'?arrow2 :arrow}  ></Image>
                </TouchableOpacity>
  
            </TouchableOpacity>
         )

         }}
         keyExtractor={(item,index)=>index.toString()}
         >
             
         </FlatList>
           
        }
        
        {
            this.state.bookData =='' && 
            <Text style={{alignSelf:'center',marginTop:50 }}>{strings.NoBooks} </Text>
        }
        </ScrollView>

        <TouchableOpacity style={styles.BottoShap} onPress={this._toggleModal}>
            <Icon name={"plus"}  size={30} color="#fff" />
        </TouchableOpacity>

       </View>
      
       <Modal
        isVisible={this.state.isModalVisible}
        onBackButtonPress={this._toggleModal}
        onBackdropPress={this._toggleModal}
        >
          <KeyboardAvoidingView behavior="height" style={{ flex: 1 , justifyContent:'center' }}>
            <View style={{ 
                backgroundColor:'white',
                padding: 10,
                borderRadius:5,
                alignSelf:'stretch'
                }}>
           
                <Text style={{marginTop:10 ,color:'black' , fontSize:20 ,fontWeight:'600',marginBottom:15,alignSelf:'center' }}>{strings.CreateBookTitle}</Text>
                <TextInput 
                    style={this.state.userNameStyle}
                    placeholder={strings.BookName}
                    returnKeyType="next"
                    onChangeText={this.onChangeBookName}
                    maxLength={30}
                    value={this.state.bookName}
                />
                <View style={this.state.coinStyle}>
                <Picker
                        selectedValue={this.state.coin}
                        onStartShouldSetResponder={true}
                        onValueChange={(itemValue)=>{this.setState({coin:itemValue})}}
                        >
                        {this.state.coins.map((coin,id)=>{
                                return(
                                    <Picker.Item label={coin} value={coin} key={id}/>   
                                )
                            })}

                        </Picker>

                </View>

                <View style={this.state.bankStyle}>
                <Picker
                        selectedValue={this.state.bank}
                        onStartShouldSetResponder={true}
                        onValueChange={this.handerChnageBank}
                        >
                        
                        <Picker.Item label={strings.defualtbank} value='' />   

                        {this.state.banks!=''&&
                        this.state.banks.map((bank,id)=>{
                                return(
                                    <Picker.Item label={bank} value={bank=='غير ذلك' ?'Other':bank} key={id}/>   
                                )
                            })}

                        </Picker>

                </View>
                
                <View style={this.state.paperNumStyle}>
                <Picker
                        selectedValue={this.state.paperNum}
                        onStartShouldSetResponder={true}
                        onValueChange={this.handerChnagepageNumber}      
                        >
                        <Picker.Item label={strings.countofpaper}  value=''/>

                        {this.state.numberofPaper.map((paperNum,id)=>{
                                return(
                                    <Picker.Item label={paperNum} value={paperNum=='غير ذلك' ?'Other':paperNum} key={id}/>   
                                )
                            })}

                        </Picker>

                </View>


                {this.state.loading===false ?
                              <View style={{flexDirection:'row' ,justifyContent:'space-evenly',alignSelf:'stretch'}}>
                              <TouchableOpacity onPress={this._toggleModal} 
                              style={styles.TouchableOpacityStyle} >
                              <Text style={styles.ButtonStyle}>{strings.cancel}</Text>
                              </TouchableOpacity>
              
                              <TouchableOpacity onPress={this._CreateNewBook} 
                              style={styles.TouchableOpacityStyle} >
                              <Text style={styles.ButtonStyle}>{strings.Create}</Text>
                              </TouchableOpacity>
                              </View>
                              :
                              <ActivityIndicator size='large' color="#007aff"/>

                }
  
            
            </View>
          </KeyboardAvoidingView>
          
        </Modal>
      
        <Modal    
        isVisible={this.state.EditModelVisible}
        onBackButtonPress={this._EditRowToggle}
        onBackdropPress={this._EditRowToggle}
        >

        <KeyboardAvoidingView behavior="height" style={{ flex: 1 , justifyContent:'center' }}>
            <View style={{ 
                backgroundColor:'white',
                padding: 10,
                borderRadius:5,
                alignSelf:'stretch'
                }}>
           
                <Text style={{marginTop:10 ,color:'black' , fontSize:20 ,fontWeight:'600',marginBottom:15,alignSelf:'center' }}>{strings.CreateBookTitle}</Text>
                <TextInput 
                    style={this.state.userNameStyle}
                    placeholder={strings.BookName}
                    returnKeyType="next"
                    value={this.state.BookName}
                    onChangeText={this.onChangeBookName}
                    maxLength={30}
                />
                <View style={this.state.coinStyle}>
                <Picker
                        selectedValue={this.state.coin}
                        onStartShouldSetResponder={true}
                        onValueChange={(itemValue)=>{this.setState({coin:itemValue})}}
                        
                        >
                        {this.state.coins.map((coin,id)=>{
                                return(
                                    <Picker.Item label={coin} value={coin} key={id}/>   
                                )
                            })}

                        </Picker>

                </View>

                <View style={this.state.bankStyle}>
                <Picker
                        selectedValue={this.state.bank}
                        onStartShouldSetResponder={true}
                        onValueChange={this.handerChnageBank}
                        >

                        <Picker.Item label={this.state.bank} value={this.state.bank} />   

                        {this.state.banks!=''&&
                        this.state.banks.map((bank,id)=>{
                                return(
                                    <Picker.Item label={bank} value={bank=='غير ذلك'?'Other':bank} key={id}/>   
                                )
                            })}

                        </Picker>

                </View>
                
                <View style={this.state.paperNumStyle}>
                <Picker
                        selectedValue={this.state.paperNum}
                        onStartShouldSetResponder={true}
                        onValueChange={this.handerChnagepageNumber}      
                        >
                        
                        <Picker.Item label={strings.countofpaper}  value={this.state.paperNum}/>
                        {this.state.numberofPaper.map((paperNum,id)=>{
                                return(
                                    <Picker.Item label={paperNum} value={paperNum=='غير ذلك' ?'Other':paperNum} key={id}/>   
                                )
                            })}

                        </Picker>

                </View>


                {this.state.loading===false ?
                              <View style={{flexDirection:'row' ,justifyContent:'space-evenly',alignSelf:'stretch'}}>
                              <TouchableOpacity onPress={this._EditRowToggle} 
                              style={styles.TouchableOpacityStyle} >
                              <Text style={styles.ButtonStyle}>Cancel</Text>
                              </TouchableOpacity>
              
                              <TouchableOpacity onPress={this._EditBookFun} 
                              style={styles.TouchableOpacityStyle} >
                              <Text style={styles.ButtonStyle}>Edit</Text>
                              </TouchableOpacity>
                              </View>
                              :
                              <ActivityIndicator size='large' color="#007aff"/>

                }
  
            
            </View>
          </KeyboardAvoidingView>
         

        </Modal>

        <DialogInput
            isDialogVisible={this.state.otherBankDialog}
            title={strings.otherBankDialogTitle}
            message={strings.otherBankDialogMessage}
            hintInput ={strings.otherBankPlaceHolder}
            submitInput={ (inputText) => {this.dismissOtherDialoge(inputText)} }
            closeDialog={ () => {this.setState({otherBankDialog:false})}}>

        </DialogInput>

        <DialogInput
            isDialogVisible={this.state.otherPageNumberDialoge}
            title={strings.paperCount}
            message={strings.paperCountMessage}
            hintInput ={strings.paperCount}
            submitInput={ (inputText) => {this.dismissOtherPageNumberDialoge(inputText)} }
            closeDialog={ () => {this.setState({otherPageNumberDialoge:false})}}
            textInputProps={{keyboardType:'number-pad'}}
            >

        </DialogInput>

                
    </View>
)
}
}

const styles=StyleSheet.create({
    contianer:{
        flex:1,
        flexDirection:"column",
    },

    homeDiv:{
        flex:1,
        justifyContent:'flex-end',
       
    },
    
    BottoShap:{
        borderWidth:2,
        alignItems:'center',
        justifyContent:'center',
        width:50,
        height:50,
        backgroundColor:'rgb(148,20,103)',
        borderRadius:50,
        position:'absolute', 
        alignSelf:'flex-end',
        zIndex: 3 ,
        bottom: '5%',
        right:'5%',
        borderColor:'rgb(148,20,103)'

    },
    InputFiled:{
        marginLeft:20,
        marginRight:10,
        borderColor:'#c0c0c0',
        borderWidth:1,
        borderRadius:5,
        backgroundColor:'#fff',
        alignSelf:'stretch',
        shadowColor:'#000',
        marginTop:10,
    },
    InputFiledError:{
        marginLeft:20,
        marginRight:10,
        borderColor:'#d50000',
        borderWidth:1,
        borderRadius:5,
        backgroundColor:'#fff',
        alignSelf:'stretch',
        shadowColor:'#000',
        marginTop:10
    },
    ButtonStyle:{
        color:'#FFF',
        fontSize:17,
        fontWeight:'500'
    },
    TouchableOpacityStyle:{
        backgroundColor:"#941467",
        width:100 ,
        height:40 ,
        alignItems: 'center',
        justifyContent:'center',
        marginTop:20,
        marginBottom:20,
        borderRadius:10
    },
    card:{
        flex:1,
        flexDirection: 'row',
        width:'95%',
        height:62,
        marginTop:10,
        padding:1,
        borderBottomLeftRadius: 50,
        borderTopLeftRadius:50,
        alignItems:'center',
        marginLeft:10,
        marginRight:10 ,
        borderRadius:10,
        backgroundColor:'white' 
    },
    ImageCard:{
        flex:0.9,
        width:60 ,
        height:60 ,
        backgroundColor:'rgb(148,20,103)' ,
        borderRadius:50 ,
        alignItems:'center', 
        justifyContent:'center',
        borderColor:'white'
    },
    BodyCard:
    {
        flex:4,
        padding:10,
        flexDirection:'row'
        
    },
    deleteCard:{
        flex:0.7,
        alignItems:'center',
        justifyContent:'center',
        height:60
     
    }
    

})

const mapToProps = dispatch =>{
    return{
        CreateNewBook:(data)=>dispatch(CreateNewBook(data)),
        GetAllBooksFun:(data)=>dispatch(GetAllBooksFun(data)),
        EditBookFun:(data)=>dispatch(EditBookFun(data)),
        DeleteBookFun:(data)=>dispatch(DeleteBookFun(data))
    }

  }

const mapToState =state =>{
    return{
      data:state.auth
    }
  }


export default connect(mapToState,mapToProps)(CreateCheque)
