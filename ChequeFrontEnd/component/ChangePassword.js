import React,{Component} from 'react'
import {View,StyleSheet,KeyboardAvoidingView,TextInput,TouchableOpacity,ActivityIndicator
    ,Text,AsyncStorage,Image} from 'react-native'
import {strings} from './Strings'
import HanderError from './inlineError'
import {updateUserPasswordFunction} from '../actions/actions'
import  {connect} from 'react-redux'
import Modal from 'react-native-modal'
import succ from '../Images/checked.png'

class ChangePassword extends Component{
    state={
        loading:true,
        oldPassword:'',
        newPassword:'',
        confirmPassword:'',

        oldPasswordStyle:styles.InputFiled,
        newPasswordStyle:styles.InputFiled,
        confirmPasswordStyle:styles.InputFiled, 
        
        Errors:{},
        userId:'',
        isModalVisible:false
    }

    componentDidMount(){
        AsyncStorage.getItem('userId').then((userId)=>{
            this.setState({userId})
        })
    }
    handlerCancel=()=>{
        this.setState({
            oldPassword:'',
            newPassword:'',
            confirmPassword:'',
    
            oldPasswordStyle:styles.InputFiled,
            newPasswordStyle:styles.InputFiled,
            confirmPasswordStyle:styles.InputFiled, 
            
            Errors:{},
        })
    }
    _renderButton(){
        if(this.state.loading){
        return (
            <View style={[styles.ElementDiv,{flexDirection:'row' ,justifyContent:'space-between',alignSelf:'stretch'}]}>
            
            <TouchableOpacity onPress={this.handlerSubmitChangePassword} 
            style={styles.TouchableOpacityStyle} >
            <Text style={styles.ButtonStyle1}>Save</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.handlerCancel} 
            style={styles.TouchableOpacityStyle} >
            <Text style={styles.ButtonStyle1}>Cancel</Text>
            </TouchableOpacity>
            </View>
        );
        }
        else{
           return(
            <ActivityIndicator size='large' color="#007aff"/>
           ); 
        }
    }
    oldPasswordChange=(oldPassword)=>{
        this.setState({oldPassword},()=>{
            if(this.setState.oldPassword==''){
                this.setState({oldPasswordStyle:styles.InputFiledError})
            }
            else{
                this.setState({oldPasswordStyle:styles.InputFiled})
            }
        })
    }
    handlernewPasswordChange=(newPassword)=>{
        this.setState({newPassword},
            ()=>{
                if(!(/(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$/).test(this.state.newPassword)){
                    this.setState({newPasswordStyle:styles.InputFiledError})
                    this.setState({
                        Errors:{...this.state.Errors,login:strings.passwordValidation}
                    })
                }
                else{
                    this.setState({newPasswordStyle:styles.InputFiled})
                    this.setState({
                        Errors:{...this.state.Errors,login:""}
                    })
                }

            })
    }

    ConfirmPasswordChange=(confirmPassword)=>{
        this.setState({confirmPassword},
            ()=>{
                if(!(/(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$/).test(this.state.confirmPassword) || this.state.newPassword!=this.state.confirmPassword){
                    this.setState({newPasswordStyle:styles.InputFiledError})
                    this.setState({confirmPasswordStyle:styles.InputFiledError})

                    this.setState({
                        Errors:{...this.state.Errors,login:strings.passwordNotMatching}
                    })
                }
                else{
                    this.setState({newPasswordStyle:styles.InputFiled})
                    this.setState({confirmPasswordStyle:styles.InputFiled})
                    this.setState({
                        Errors:{...this.state.Errors,login:""}
                    })
                }

            })
    }


    handlerSubmitChangePassword =() =>{
        if(!this.state.oldPassword || !this.state.newPassword || !this.state.confirmPassword ){
            this.setState({Errors:{...this.state.Errors,login:strings.PasswordMobileError}})

            if(!this.state.oldPassword){
                this.setState({oldPasswordStyle:styles.InputFiledError})
            }
            else{
                this.setState({oldPasswordStyle:styles.InputFiled})
                this.setState({Errors:{...this.state.Errors,login:''}})

            }
            if(!this.state.newPassword || !(/(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$/).test(this.state.newPassword)){
                this.setState({newPasswordStyle:styles.InputFiledError})
            }
            else{
                this.setState({newPasswordStyle:styles.InputFiled})
                this.setState({Errors:{...this.state.Errors,login:''}})
            }
            if(!this.state.confirmPassword || this.state.newPassword!=this.state.confirmPassword){
                this.setState({newPasswordStyle:styles.InputFiledError})
                this.setState({confirmPasswordStyle:styles.InputFiledError})
            }else{
                this.setState({newPasswordStyle:styles.InputFiled})
                this.setState({confirmPasswordStyle:styles.InputFiled})
                this.setState({Errors:{...this.state.Errors,login:''}})
            }

        }

        else if(!(/(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$/).test(this.state.newPassword)){
            this.setState({newPasswordStyle:styles.InputFiledError})
            this.setState({
                Errors:{...this.state.Errors,login:strings.passwordValidation}
            })
        }
        else if(this.state.newPassword != this.state.confirmPassword){
            this.setState({newPasswordStyle:styles.InputFiledError})
            this.setState({confirmPasswordStyle:styles.InputFiledError})

            this.setState({
                Errors:{...this.state.Errors,login:strings.passwordNotMatching}
            })
        }
        else{
            var data={user_id:this.state.userId,user_password:this.state.oldPassword , new_password:this.state.newPassword }
            this.setState({loading:false})
            this.props.updatePassword(data).then(res=>{
                if(this.props.data.data.success){
                    this.setState({isModalVisible:true})
                    this.setState({Errors:{...this.state.Errors,login:''}})
                    setTimeout(function(){
                        this.setState({oldPassword:''})
                        this.setState({newPassword:'' })
                        this.setState({confirmPassword:''})
                        this.setState({loading:true})
                        this.setState({isModalVisible:false})
                        }.bind(this),1400)
                }
                else{
                    this.setState({Errors:{...this.state.Errors,login:'Incorect old Password'}})
                    if(this.state.oldPassword == this.state.newPassword){
                        this.setState({Errors:{...this.state.Errors,login:'old password is the same of new password'}})
                    }
                    this.setState({loading:false})
                    setTimeout(function(){
                        this.setState({loading:true})
                        }.bind(this),1000)
                }
            })
           
        }

        
    }

    render(){
        return(
            <KeyboardAvoidingView  style={styles.contianer} >

                <View style={styles.divHome}>
                    <View style={styles.ElementDiv}>
                        <TextInput 
                        style={this.state.oldPasswordStyle}
                        placeholder='Old password'
                        returnKeyType="next"
                        onChangeText={this.oldPasswordChange}
                        value={this.state.oldPassword}
                        secureTextEntry={true}

                        />
                    </View>
                    <View style={styles.ElementDiv}>
                        <TextInput 
                        style={this.state.newPasswordStyle}
                        placeholder='New password'
                        returnKeyType="next"
                        onChangeText={this.handlernewPasswordChange}
                        value={this.state.newPassword}
                        secureTextEntry={true}

                        />
                    </View>
                    <View style={styles.ElementDiv}>
                        <TextInput 
                        style={this.state.confirmPasswordStyle}
                        placeholder='Confirm password'
                        returnKeyType="next"
                        onChangeText={this.ConfirmPasswordChange}
                        value={this.state.confirmPassword}
                        secureTextEntry={true}

                        />
                    </View>
                    <View><HanderError>{this.state.Errors.login}</HanderError></View>
                    <View style={styles.ButtonStyle}>
                        {this._renderButton()}
                    </View>
                </View>
                <Modal
                       isVisible={this.state.isModalVisible}
                       onBackButtonPress={()=>{this.setState({isModalVisible:false})}}
                       onBackdropPress={()=>{this.setState({isModalVisible:false})}}
                >
                    <View style={{ flex: 1 , justifyContent:'center' }}>
                        <View style={{  backgroundColor:'white',
                                        padding: 10,
                                        borderRadius:5,
                                        width:'60%',
                                        height:180,
                                        alignSelf:'center',
                                        alignItems:'center'
                                        }}>
                                <Image source={succ} ></Image>
                                <Text style={{fontSize:17 ,marginTop:20,width:'80%',
                                marginLeft:'10%',marginRight:'10%',color:'#000' , alignItems:"center"}}>your password has changed successfly</Text>
                        </View>
                    </View>
                </Modal>

            </KeyboardAvoidingView>
        );
    }
}


const styles=StyleSheet.create({
    contianer:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
    },
    divHome:{
        flex:4,
        justifyContent:'center',
        alignItems:'center',
        width:'80%'
    },
    divEnd:{
        flex:0.2,
        justifyContent:'flex-end',
        alignItems:'center',
    },
    InputFiled:{
        marginLeft:5,
        marginRight:5,
        borderColor:'#c0c0c0',
        borderWidth:1,
        borderRadius:5,
        backgroundColor:'#fff',
        alignSelf:'stretch',
        shadowColor:'#000', 
    },
    InputFiledError:{
        marginLeft:5,
        marginRight:5,
        borderColor:'#d50000',
        borderWidth:1,
        borderRadius:5,
        backgroundColor:'#fff',
        alignSelf:'stretch',
        shadowColor:'#000',
    },
    ElementDiv:{
        marginTop:10,
        marginBottom:10,
        alignSelf:'stretch',   
    },
    TextStyle:{
        alignSelf:'center',
        color:"#808080",
        fontSize:16,
        fontWeight:'600',
        paddingTop:10,
        paddingBottom:10,
    },
    TouchableOpacityStyle:{
        backgroundColor:"#941467",
        width:100 ,
        height:40 ,
        alignItems: 'center',
        justifyContent:'center',
        marginTop:20,
        marginBottom:20,
        borderRadius:5
    },
    ButtonStyle:{
        color:'#FFF',
        fontSize:17,
        fontWeight:'500',
        width:'80%'
    },
    ButtonStyle1:{
        color:'#FFF',
        fontSize:17,
        fontWeight:'500',
    }
})

const maptToProps =dispatch =>{
    return{
        updatePassword:(data)=>dispatch(updateUserPasswordFunction(data)),
        
        
    }
}

const mapToState =state =>{
    return{
      data:state.auth
    }
  }


export default connect(mapToState,maptToProps)(ChangePassword)
