import React, { PureComponent } from "react";
import { ScrollView } from "react-native";
import { DrawerItems, SafeAreaView,View,Text,TouchableOpacity } from "react-navigation";
import Icon from 'react-native-vector-icons/FontAwesome';

handerLogout=()=>{
    Alert.alert(
      'LogOutMessage',
      'Are you sure,you want to Logout app?',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK', onPress: () => {//AsyncStorage.removeItem("userId")
          navigation.dispatch(NavigationActions.navigate({ routeName: 'Login' }))
        }
        },
       
      ],
      {cancelable: false},
    );
  }
class BurgerMenu extends PureComponent {
  render() {
    return (
        <SafeAreaView style={{flex:1}}>
      <View style={{backgroundColor:'white',alignItems:'center',justifyContent:'center',flexDirection:'row',flex:.5}}>
      <Icon name="bank" size={20}></Icon>
      <Text style={{fontWeight:'bold',fontSize:15,color:'black'}} >CHEQUES</Text>
      </View>
      <ScrollView>
      <DrawerItems {...this.props} />
      </ScrollView>
      <View style={{flex:0.5,backgroundColor:'white',alignItems:'flex-start',justifyContent:'flex-start',flexDirection:'row'}}>
      <Icon name="sign-out" resizeMode="contain" size={20} style={{marginLeft:15}}/>
      <TouchableOpacity style={{marginLeft:10}} onPress={this.handerLogout} ><Text style={{fontWeight:'bold',fontSize:15,color:'black'}}>LogOut</Text></TouchableOpacity>
      </View>
    </SafeAreaView>
    );
  }
}

export default BurgerMenu;