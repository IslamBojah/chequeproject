import LocalizedStrings from 'react-native-localization';
 
 
export let strings = new LocalizedStrings({
en:{
    mobleNumber:"Mobile Number",
    password:"Password",
    LOGIN:'LOGIN',
    CreatedAccount:'Create New account',
    ForgetPassword:"Forget Password",
    PasswordMobileError:'Please Fill All Fields',
    mobileError:"Not valid mobile number ",
    passwordError:"Invalid mobile number or password",
    username:"Username",
    newAccount:'Create new account ',
    confirmPassword:'Confirm password',
    SignUp:'Sign Up',
    passwordNotMatching:'Passwords are not matching',
    userNameError:'Username must be of characters only ',
    passwordValidation:'Password must contain at least charcter, number and symbol more than 8 places',
    titleApp:'My Cheques',
    ArabicLan:'Arabic',
    EnglishLan:'English',
    CreateBookTitle:'Book Information',
    BookName:'Book Name',
    currency:'currency unit',
    defualtbank:'select bank',
    defualtchequeStatus:'Additional Information',
    countofpaper:'paper count',
    JD:'JD',
    USD:'USD',
    ILS:'NIS',
    banks:['Bank of Palestine',' Arab Islamic Bank','Palestine Islamic Bank','Palestine Investment Bank',' Al Quds Bank','The National Bank TNB',' Safa Bank','Arab Bank','Cairo Amman Bank','Bank of Jordan','Housing Bank',' Egyptian Arab Land Bank','Jordan Ahli Bank','Commercial Bank of Jordan','Jordan Kuwait Bank' , 'Other'],
    NoBooks:'No books for this user',
    EditBook:'Edit Book',
    DeleteItem:'Delete',
    EditItem:'Edit',
    simpleTextchequeBook:'Pick a date',
    ValueChequeBook:'Value',
    nameRecipientChecqueBook:'Payee name',
    chequeNumChequeBook:'Cheque number',
    PaidForPrimaryCheque:'Paid for primary payee',
    crossedCheque:'Crossed cheque',
    createCheque:'Create New Cheque',
    ReceveCheque:'ReceveCheque',
    chequeValue:'chequeValue',
    pickerDate:'pick a date',
    Nameofthecheckrecipient:'Payer person',
    chequeNumber:'cheque Number',
    ChequeStatus:['Normal','Endorsed','Paid for primary payee'],
    crosscheque:"Crossed cheque",
    Checkbooks:'Cheques books',
    Calendar:'Calendar',
    ReceiveCheque:'Received Cheques',
    otherBankDialogTitle:'Bank name',
    otherBankDialogMessage:'Type bank name , please ',
    otherBankPlaceHolder:'Bank name',
    numberofPaper:['10','15','20','30','35','Other'],
    Home:'Home',
    ChangePassword:'Password',
    PhoneNumber:'Phone Number',
    logout:'Logout',
    ConfirmDeleteItem :'Are you sure to Delete this item',
    ok:'Ok',
    cancel:'Cancel',
    paperCount:'Paper Count',
    paperCountMessage:'Type paper count please ! ',
    Create:'Create',
    Done:'Done',
    Bounded:'Bounded',
    ChequesCalender:'Created Cheque',
    RecivedCalender:'Recieved Cheque',
    TodaysChequeNotification:'You Have a Cheque Today ',
    ThreeDaysChequeNotification:'You Have a Cheque after three days ',
    weekChequeNotification:'You Have a Cheque after one week ',



 },
 ar: {
   mobleNumber:"رقم الهاتف",
   password:"كلمة السر",
   LOGIN:"تسجيل الدخول",
   CreatedAccount:"انشاء حساب جديد",
   ForgetPassword:"هل نسيت كلمة المرور",
   PasswordMobileError:'يرجى تعبئة جميع الحقول ',
   mobileError:"رقم الهاتف غير صحيح ",
   passwordError:"كلمة السر او رقم الهاتف غير صحيحة",
   username:'اسم المستخدم',
   newAccount:'انشاء حساب جديد',
   confirmPassword:'تأكيد كلمة السر ',
   SignUp:'إنشاء حساب',
   passwordNotMatching:'كلمة السر غير متطابقة',
   userNameError:'اسم المستخدم يجب ان يتكون من الحروف فقط',
   passwordValidation:'كلمة السر يجب ان تتكون على الاقل من احرف وارقام ورموز من 8 خانات',
  titleApp:'شيكاتي',
  ArabicLan:'العربية',
  EnglishLan:'الانجليزية',
  CreateBookTitle:'معلومات الدفتر ',
  BookName:'اسم الدفتر ',
  JD:'دينار',
  USD:'دولار',
  ILS:'شيكل',
  currency:'العملة',
  defualtbank:'اختر اسم البنك',
  countofpaper:'عدد الاوراق',
  banks:['بنك الأردن الكويتي','البنك التجاري الأردني','البنك الأهلي الأردني','البنك العقاري المصري العربي','بنك الاسكان للتجارة والتمويل','بنك الأردن','بنك القاهرة عمان','البنك العربي','مصرف الصفا الاسلامي','البنك الوطني','بنك القدس','بنك الاستثمار الفلسطيني','البنك الإسلامي الفلسطيني','البنك الإسلامي العربي','بنك فلسطين' ,'غير ذلك'],
  NoBooks:'لا يوجد دفاتر لهذا لمستخدم',
  EditBook:'تعديل الدفتر',
  DeleteItem:'حذف',
  EditItem:'تعديل',
  simpleTextchequeBook:'اختر التاريخ',
  ValueChequeBook:'قيمة الشك',
  nameRecipientChecqueBook:' اسم المستلم',
  chequeNumChequeBook:'رقم الشيك',
  PaidForPrimaryCheque:'يصرف من الطرف الاول',
  crossedCheque:'شك مشحط',
  createCheque:'إنشاء شك جديد',
  ReceveCheque:'أستلام شيك',
  chequeValue:'قيمة شيك',
  pickerDate:'التاريخ',
  Nameofthecheckrecipient:'ادخل اسم مستلم الشيك',
  chequeNumber:'ادخل رقم الشيك',
  ChequeStatus:['غير محدد','يصرف من طرف واحد','مجيير','مشحط'],
  defualtchequeStatus:'حالة الشيك',
  crosscheque:"شك مشحط",
  Checkbooks:'دفاتر الشيكات',
  Calendar:'التقويم',
  ReceiveCheque:'الشيكات المستلمة',
  otherBankDialogTitle:'اسم البنك',
  otherBankDialogMessage:'اكتب اسم البنك',
  otherBankPlaceHolder:'اسم البنك',
  numberofPaper:['10','15','20','30','35','غير ذلك'],
  Home:'الرئيسية',
  ChangePassword:'كلمة السر',
  PhoneNumber:'رقم الهاتف',
  logout:'تسجيل الخروج',
  ConfirmDeleteItem:'هل تريد حذف هذا العنصر بالتاكيد',
  ok:'نعم',
  cancel:'الغاء',
  paperCount:'عدد الاوراق',
  paperCountMessage:'اكتب عدد الاوراق لو سمحت',
  Create:'انشاء',
  Done:'تم صرفه',
  Bounded:'شك مرجع',
  ChequesCalender:'شك منشأ',
  RecivedCalender:'شك مستلم',
  TodaysChequeNotification:'لديك شك اليوم',
  ThreeDaysChequeNotification:'لديك شك بعد ثلاثة أيام ',
  weekChequeNotification:'لديك شك بعد أسبوع'
 }
});