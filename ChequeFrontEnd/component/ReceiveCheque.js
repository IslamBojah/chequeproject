import React,{Component} from 'react'
import {View,StyleSheet,Text,TouchableOpacity,TouchableWithoutFeedback,KeyboardAvoidingView,
DatePickerAndroid,TextInput,Picker,ScrollView,CheckBox,AsyncStorage,
ActivityIndicator,FlatList,Alert} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import NotifService from '../notification/NotifService '
import Modal from 'react-native-modal'
import {strings} from './Strings'
import {AddChequeReciveFun,ChequeReciveDatafun,DeleteReciveDatafun,
    UpdateReciveChequefun,UpdateReciveChequestatusfun} from '../actions/actions'
import  {connect} from 'react-redux'
import DialogInput from 'react-native-dialog-input'

class ReceiveCheque  extends Component{
    constructor(props) {
        super(props);
        this.state = {
            simpleDate: new Date(Date.now()),
            simpleText: strings.pickerDate,
            coins:[strings.ILS , strings.USD , strings.JD ],
          

            bankStyle:styles.InputFiled,
            coinStyle:styles.InputFiled,
            pickerDateStyle:styles.InputFiled,
            chequeValueStyle:styles.InputFiled,
            namechequeRecStyle:styles.InputFiled,
            chequeNumberStyle:styles.InputFiled,
            chequeStautsStyle:styles.InputFiled,
            isModalVisible:false,

            banks:strings.banks,
            chequeStatus:strings.ChequeStatus,

            bank:'',
            coin:strings.ILS,
            chequeValue:'',
            nameChequeValue:'',
            chequeNumberValue:'',
            chequeStatusValue:'',
            user_id:'',
            id_item:'',
          
            crossCheque:false,
            loading:false,
            data:'',
            statusModal:1,

            test:[{date:'04/4/19',valueCheque:'500',coin:'Jd',ReciptName:'mohammad',chequeNumber:'123334',bankName:'palestineBank'}],

            editable:false,

            editId:'',

            otherDialog:false,

        };

    

    }

   componentDidMount(){
    AsyncStorage.getItem('userId').then((user_id)=>{
 
        this.setState({user_id},()=>{
            this.props.ChequeReciveDatafun({user_id:this.state.user_id}).then(res=>this.setState({data:Array.prototype.reverse.call(this.props.data.data)}))
        }) 
        
    })

   
   }

  

    _toggleModal = () =>{
        this.setState({ isModalVisible: !this.state.isModalVisible });
        this.setState({chequeValue:'',chequeNumberValue:'',coin:strings.ILS,nameChequeValue:'',bank:'',
        chequeStatusValue:'',simpleText:strings.pickerDate,statusModal:0,
             bankStyle:styles.InputFiled,
            coinStyle:styles.InputFiled,
            pickerDateStyle:styles.InputFiled,
            chequeValueStyle:styles.InputFiled,
            namechequeRecStyle:styles.InputFiled,
            chequeNumberStyle:styles.InputFiled,
            chequeStautsStyle:styles.InputFiled,
            crossCheque:false,
            editable:false
        
        })
    }

    showPicker = async (stateKey, options) => {
        try {
          var newState = {};
          const {action, year, month, day} = await DatePickerAndroid.open(options);
          if (action === DatePickerAndroid.dismissedAction) {
            newState[stateKey + 'Text'] =this.state.simpleText;
          } else {

            var date = new Date(year, month, day);
            newState[stateKey + 'Text'] = date.toLocaleDateString()
            newState[stateKey + 'Date'] = date;
          }
          
          this.setState(newState);
          var d = String(date.getDate()).padStart(2, '0');
            var m = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
            var y = date.getFullYear();
            this.setState({pickerDateStyle:styles.InputFiled})
            this.setState({simpleText:y+'-'+m+'-'+d})

        } catch ({code, message}) {
          console.warn(`Error in example '${stateKey}': `, message);
        }
      };

      /*--------------------------HandlechangeInput---------------------*/

      handlerChequeValue=(value)=>{
        this.setState({...this.state.chequeValue,
            chequeValue:value
        },()=>{
            if(this.state.chequeValue>0){
                this.setState({chequeValueStyle:styles.InputFiled})
            }else{
                this.setState({chequeValueStyle:styles.InputFiledError})
            }
        })
      }
      handlerChequeNumber=(value)=>{
        this.setState({...this.state.chequeNumberValue,
            chequeNumberValue:value
        },()=>{
            if((/^([0-9]{1})*$/).test(this.state.chequeNumberValue)){
                this.setState({chequeNumberStyle:styles.InputFiled})
            }else{
                this.setState({chequeNumberStyle:styles.InputFiledError})
            }
        })
      }
      handlerChequeName=(value)=>{
        this.setState({...this.state.nameChequeValue,
            nameChequeValue:value
        },()=>{
            if((/^([\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z ]+[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z-_ ])*$/).test(this.state.nameChequeValue)){
                this.setState({namechequeRecStyle:styles.InputFiled})
            }else{
                this.setState({namechequeRecStyle:styles.InputFiledError})
            }
        })
      }

      handerChnageCoins=(value)=>{
          this.setState({...this.state.coin,
            coin:value
        },()=>{
            if (this.state.coin !=""){
                this.setState({coinStyle:styles.InputFiled})
            }
            else{
                this.setState({coinStyle:styles.InputFiledError})
            }
        })
        
      }

      handerChnageBank=(value)=>{
        if(value=='Other'){
            this.setState({otherDialog:true})
        }
        else{
            this.setState({...this.state.bank,
                bank:value
            },()=>{
                if (this.state.bank !=""){
                    this.setState({bankStyle:styles.InputFiled})
                }
                else{
                    this.setState({bankStyle:styles.InputFiledError})
                }
            })
        }
    }
 
    dismissOtherDialoge=(value)=>{
        if(value != ''){
            this.setState({bank:value},()=>{
                this.setState({otherDialog:false})
                this.handerChnageBank(value)
                this.state.banks.unshift(value)
            })
        }
        
    }
      hander_Validation=()=>{
        if (!this.state.simpleDate || !this.state.chequeValue ||!this.state.coin || !this.state.nameChequeValue 
            || !this.state.chequeNumberValue || !this.state.bank){
        
            if(this.state.simpleText ==strings.pickerDate){
               
                this.setState({pickerDateStyle:styles.InputFiledError});
            }
            else{
               
                this.setState({pickerDateStyle:styles.InputFiled});
            }
            if(!this.state.chequeValue || this.state.chequeValue <=0){
                this.setState({chequeValueStyle:styles.InputFiledError});
            }
            else{
                this.setState({chequeValueStyle:styles.InputFiled}); 
            }
            if(!this.state.coin){
                this.setState({coinStyle:styles.InputFiledError});
            }
            else{
                this.setState({coinStyle:styles.InputFiled});
                }
    
            if(!this.state.nameChequeValue || !(/^([\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z ]+[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z-_ ])*$/).test(this.state.nameChequeValue)){
                this.setState({namechequeRecStyle:styles.InputFiledError})
            }
            else{
                this.setState({namechequeRecStyle:styles.InputFiled})
            }
            if(!this.state.chequeNumberValue || !(/^([0-9]{1})*$/).test(this.state.chequeNumberValue)){
                this.setState({chequeNumberStyle:styles.InputFiledError})
            }
            else{
                this.setState({chequeNumberStyle:styles.InputFiled})
            }
            if(!this.state.bank){
                this.setState({bankStyle:styles.InputFiledError})
            }
            else{
                this.setState({bankStyle:styles.InputFiled})
            }

       

        }
         else if  (this.state.chequeValue <=0){
            this.setState({chequeValueStyle:styles.InputFiledError})
        }
       else  if(!(/^([\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z ]+[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z-_ ])*$/).test(this.state.nameChequeValue)){
            this.setState({namechequeRecStyle:styles.InputFiledError})
        }
      else  if(!(/^([0-9]{1})*$/).test(this.state.chequeNumberValue)){
            this.setState({chequeNumberStyle:styles.InputFiledError})
        }    
        else{
            if(this.state.statusModal==0){
            this.setState({loading:true,statusModal:0})

            var crossVal=''
            if(this.state.crossCheque == false){
                crossVal='0'
            }
            else{
                crossVal='1'
            }

            var data={user_id:this.state.user_id,bank_name:this.state.bank,
            date:this.state.simpleText,value:this.state.chequeValue,from_who:this.state.nameChequeValue,
            cheque_no:this.state.chequeNumberValue,cheque_status:this.state.chequeStatusValue,coin:this.state.coin,
            is_crossed:crossVal}
            
            this.props.reciveCheque(data).then(res=>
                this.state.data.unshift(this.props.data.data[0])
                )
                    /*------- edit -----*/
            }
            else{
                var data={id:this.state.id_item,bank_name:this.state.bank,
                    date:this.state.simpleText,value:this.state.chequeValue,from_who:this.state.nameChequeValue,
                    cheque_no:this.state.chequeNumberValue,cheque_status:this.state.chequeStatusValue,coin:this.state.coin,
                    is_crossed:this.state.crossCheque}

                    this.props.UpdateReciveChequefun(data).then(res=>{
                        this.state.data.map((cheque)=>{
                            if(cheque.id===this.state.id_item){
                            
                                cheque.bank_name=this.state.bank,
                                cheque.date=this.state.simpleText,
                                cheque.value=this.state.chequeValue,
                                cheque.coin=this.state.coin,
                                cheque.from_who=this.state.nameChequeValue,
                                cheque.cheque_no=this.state.chequeNumberValue,
                                cheque.cheque_status=this.state.chequeStatusValue,
                                cheque.is_crossed=this.state.crossCheque
    
    
                            }
                        })
                    })
 
            }

            setTimeout(function(){
                this.setState({loading:false})
                this.setState({ isModalVisible: !this.state.isModalVisible });
                this.setState({crossCheque:false})
            }.bind(this),1000)
           

        }
    } 


    handerCancel=()=>{

        this.setState({chequeValue:'',chequeNumberValue:'',coin:'',nameChequeValue:'',bank:'',
        chequeStatusValue:'',simpleText:strings.pickerDate,
             bankStyle:styles.InputFiled,
            coinStyle:styles.InputFiled,
            pickerDateStyle:styles.InputFiled,
            chequeValueStyle:styles.InputFiled,
            namechequeRecStyle:styles.InputFiled,
            chequeNumberStyle:styles.InputFiled,
            chequeStautsStyle:styles.InputFiled,
            crossCheque:false,isModalVisible:false , editable:false})
    }
    
   handerEditDelete=(rowData)=>{
     
    Alert.alert(
        strings.DeleteItem,
        strings.ConfirmDeleteItem,
        [
          {text:strings.cancel, onPress: () => console.log('Ask me later pressed'),
          style: 'cancel',
        },
 
          {text:strings.DeleteItem, onPress: () =>{
              this.props.DeleteReciveDatafun(rowData.id)
              const newData=this.state.data.filter((checkes)=>{
                return checkes.id !=rowData.id
              })
              this.setState({data:newData})
        }},
        ],
        {cancelable: false},
      );
   } 
handlerEditCheque=(rowData)=>{
    this.setState({ isModalVisible:true,id_item:rowData.id ,bank:rowData.bank_name,chequeValue:rowData.value,
        nameChequeValue:rowData.from_who,coin:rowData.coin,chequeNumberValue:rowData.cheque_no,
        statusModal:1,crossCheque:rowData.cheque_status,user_id:rowData.id,simpleText:rowData.date})

        if(rowData.is_crossed=='0'){
            this.setState({crossCheque:false})
        }
        else{
            this.setState({crossCheque:true})
        }


        const today=new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();   
        
        var date= yyyy+'-'+mm+'-'+dd

        if(date >= rowData.date){
            this.setState({editable:true})
        }
        this.setState({editId:rowData.id})
    }

handlerDoneCheque=()=>{
    var data={status:'2' , id:this.state.id_item}
    this.props.UpdateReciveChequestatusfun(data).then(res=>{
      //  alert(this.state.id_item)
        const newData=this.state.data.filter((checkes)=>{
            return checkes.id !=this.state.id_item
          })
        this.setState({isModalVisible:!this.state.isModalVisible})
        this.setState({data:newData})
        this.handerCancel
        this.setState({editable:false})
    })
}
handlerBoundedCheque=()=>{
    var data={status:'1' , id:this.state.id_item}
    this.props.UpdateReciveChequestatusfun(data).then(res=>{
      //  alert(this.state.id_item)
        this.state.data.map((checkes)=>{
           if(checkes.id ==this.state.id_item){
                checkes.status='1'
            }
          })
        this.setState({isModalVisible:!this.state.isModalVisible})
        this.handerCancel
        this.setState({editable:false})
    })
}
    

render(){

return(
    <View style={styles.contianer}>
    <View style={styles.homeDiv}>
    {this.state.data!='' &&  Object.keys(this.state.data).length>0 ?
    <FlatList
        data={this.state.data}
        renderItem={({item:rowData,index})=>{
            return(
                <TouchableOpacity onLongPress={()=>this.handerEditDelete(rowData)} onPress={()=>this.handlerEditCheque(rowData)}>
                <View style={rowData.status=='1' ?[styles.card,{borderWidth:1,borderColor:'#d50000'}] :styles.card} key={index} > 
                        <View style={styles.uppDiv}>
                           <Text style={{fontSize:16,fontWeight:'bold',marginLeft:10,marginTop:5}}>{rowData.from_who}</Text>
                            <Text style={{fontSize:16,fontWeight:'bold', fontStyle: 'italic',marginRight:5,marginTop:5}}>{rowData.date}</Text>
                        </View>
                        <View style={styles.belowDiv}>
                           <Text style={{fontSize:14,marginLeft:5,marginTop:5}}>{rowData.bank_name}</Text>
                            {rowData.status=='1' &&
                            <Text style={{color:'#d50000' , fontWeight:'bold'}}>Bounded</Text>
                        }
                            <Text style={{fontSize:14,marginRight:10,marginTop:5,fontWeight:'bold',color: "#d50000"}}>{rowData.value} {rowData.coin} </Text>
                        </View>
                </View>
                </TouchableOpacity>
            )
        }}
        keyExtractor = {(item, index) => index.toString()}
        extraData={this.state}
       
        >
        
            
        </FlatList>
        :
        <Text style={{alignSelf:'center',marginTop:50 }}>No received cheques available yet </Text>
    }
        
   
        <Modal
        isVisible={this.state.isModalVisible}
        onBackButtonPress={this._toggleModal}
        onBackdropPress={()=>null}
        >

    
        <ScrollView contentContainerStyle={styles.contentContainer} >
                
             <View style={styles.ModalStyle}>
            <Text style={{marginTop:10 ,color:'black' , fontSize:20 ,fontWeight:'600',marginBottom:15,alignSelf:'center' }}>{strings.ReceveCheque}</Text>
                
        <TouchableWithoutFeedback
                
                disabled={this.state.editable==true ? true:false}
                onPress={this.showPicker.bind(this, 'simple', {date: this.state.simpleDate})}>
                <View style={[this.state.pickerDateStyle,{height:40}]}><Text style={{fontSize:15}}>{this.state.simpleText}</Text></View> 
          </TouchableWithoutFeedback>
          
          <View style={{ flexDirection:'row'}}> 
          <View style={{flex:1}}>
                        <TextInput editable={this.state.editable==true ? false :true}  keyboardType="number-pad" value={this.state.chequeValue} placeholder={strings.chequeValue} style={this.state.chequeValueStyle}  onChangeText={this.handlerChequeValue}/>
                    </View>
                    <View style={[{flex:1},this.state.coinStyle]}  >
                    <Picker
                        selectedValue={this.state.coin}
                        onStartShouldSetResponder={true}
                        onValueChange={this.handerChnageCoins}
                        enabled={this.state.editable==true ? false:true }  
                        > 

                        {this.state.coins.map((coin,id)=>{
                                return(
                                    <Picker.Item label={coin} value={coin} key={id}/>   
                                )
                            })}

                        </Picker>
                    </View>

               
          </View>

        
          <TextInput placeholder={strings.Nameofthecheckrecipient} style={this.state.namechequeRecStyle} 
           onChangeText={this.handlerChequeName}  value={this.state.nameChequeValue}  
           maxLength={50}
           editable={this.state.editable==true ? false :true}
           
           />

         

          <TextInput placeholder={strings.chequeNumber} keyboardType="number-pad" style={this.state.chequeNumberStyle}
          onChangeText={this.handlerChequeNumber} value={this.state.chequeNumberValue}
          editable={this.state.editable==true ? false :true}
          />
         
          <View style={[{flex:1},this.state.bankStyle]}>
                <Picker
                        selectedValue={this.state.bank}
                        onStartShouldSetResponder={true}
                        onValueChange={this.handerChnageBank}
                        enabled={this.state.editable==true ? false:true }  

                        >
                        
                        <Picker.Item label={strings.defualtbank} value='' />   

                        {this.state.banks!=''&&
                        this.state.banks.map((bank,id)=>{
                                return(
                                    <Picker.Item label={bank} value={bank=='غير ذلك' ?'Other':bank} key={id}/>   
                                )
                            })}
                            
                        </Picker>
      

                </View>

                <View style={[{flex:1},this.state.chequeStautsStyle]}>
                <Picker
                        selectedValue={this.state.chequeStatusValue}
                        onStartShouldSetResponder={true}
                        onValueChange={(itemValue)=>{this.setState({chequeStatusValue:itemValue})}}
                        enabled={this.state.editable==true ? false:true }  

                        >
                        
                        <Picker.Item label={strings.defualtchequeStatus} value='' />   

                        {this.state.banks!=''&&
                        this.state.chequeStatus.map((bank,id)=>{
                                return(
                                    <Picker.Item label={bank} value={bank} key={id}/>   
                                )
                            })}

                        </Picker>

                        

                </View>
                <View style={[{flexDirection:"row"},styles.CheckBox]}> 
                <CheckBox  value={this.state.crossCheque}
                 onValueChange={()=>this.setState({crossCheque:!this.state.crossCheque})}
                 disabled={this.state.editable==true?true:false}
                 ></CheckBox>
                <Text style={{marginTop:5}}>{strings.crosscheque}</Text>
                
                </View>
                            
                {this.state.editable==false ?
                <View>                
                    {this.state.loading ==false ? 
                
                        <View style={{flexDirection:'row' ,justifyContent:'space-evenly',alignSelf:'stretch'}}>
                            <TouchableOpacity onPress={this.handerCancel} 
                            style={styles.TouchableOpacityStyle} >
                            <Text style={styles.ButtonStyle}>{strings.cancel}</Text>
                            </TouchableOpacity>
                            {this.state.statusModal == 0? 
                            <TouchableOpacity onPress={this.hander_Validation} 
                            style={styles.TouchableOpacityStyle} >
                            <Text style={styles.ButtonStyle}>{strings.Create}</Text>
                            </TouchableOpacity>
                                :
                                <TouchableOpacity onPress={this.hander_Validation} 
                                style={styles.TouchableOpacityStyle} >
                                <Text style={styles.ButtonStyle}>{strings.EditItem}</Text>
                                </TouchableOpacity>
                            }
                                
                            </View>
                            :
                            <ActivityIndicator size='large' color="#007aff"/>
                            }
                </View>
                :
                <View style={{flexDirection:'row' ,justifyContent:'space-evenly',alignSelf:'stretch'}}>

                <TouchableOpacity onPress={this.handlerDoneCheque} 
                style={styles.TouchableOpacityStyle} >
                <Text style={styles.ButtonStyle}>{strings.Done}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.handlerBoundedCheque} 
                style={styles.TouchableOpacityStyle} >
                <Text style={styles.ButtonStyle}>{strings.Bounded}</Text>
                </TouchableOpacity>

                </View>
                }
            </View>

         </ScrollView>
        
        </Modal>
    </View>
   
        <DialogInput
            isDialogVisible={this.state.otherDialog}
            title={strings.otherBankDialogTitle}
            message={strings.otherBankDialogMessage}
            hintInput ={strings.otherBankPlaceHolder}
            submitInput={ (inputText) => {this.dismissOtherDialoge(inputText)} }
            closeDialog={ () => {this.setState({otherDialog:false})}}>

        </DialogInput>


    <TouchableOpacity style={styles.BottoShap}  onPress={this._toggleModal}>
<Icon name={"plus"}  size={30} color="#fff" />
</TouchableOpacity>

    
    

 </View>
)
}
}
const styles=StyleSheet.create({
    contianer:{
        flex:1,
        flexDirection:"column",
    },
    homeDiv:{
        flex:1,
       
    },
    BottoShap:{
        borderWidth:2,
        alignItems:'center',
        justifyContent:'center',
        width:50,
        height:50,
        backgroundColor:'rgb(148,20,103)',
        borderRadius:50,
        position:'absolute', 
        alignSelf:'flex-end',
        zIndex: 3 ,
        bottom: '5%',
        right:'5%',
        borderColor:'rgb(148,20,103)'
    },
    button: {
        borderWidth: 1,
        borderColor: "#000000",
        margin: 5,
        padding: 5,
        width: "70%",
        backgroundColor: "#DDDDDD",
        borderRadius: 5,
      },
      ModalStyle:{ 
        backgroundColor:'white',
        padding: 10,
        borderRadius:5,
        alignSelf:'stretch'
      },
      TouchableOpacityStyle:{
        backgroundColor:"#941467",
        width:100 ,
        height:40 ,
        alignItems: 'center',
        justifyContent:'center',
        marginTop:20,
        marginBottom:20,
        borderRadius:10
    },
    ButtonStyle:{
        color:'#FFF',
        fontSize:17,
        fontWeight:'500'
    },
    InputFiled:{
        marginLeft:20,
        marginRight:10,
        borderColor:'#c0c0c0',
        borderWidth:1,
        borderRadius:5,
        backgroundColor:'#fff',
        alignSelf:'stretch',
        shadowColor:'#000',
        marginTop:10,       
    },
    InputFiledError:{
        marginLeft:20,
        marginRight:10,
        borderColor:'#d50000',
        borderWidth:1,
        borderRadius:5,
        backgroundColor:'#fff',
        alignSelf:'stretch',
        shadowColor:'#000',
        marginTop:10
    },
    ImageCard:{
        flex:0.9,
        width:60 ,
        height:60 ,
        backgroundColor:'rgb(148,20,103)' ,
        borderRadius:50 ,
        alignItems:'center', 
        justifyContent:'center',
        borderColor:'white'
    },
    contentContainer: {
        paddingVertical: 20
      },
      CheckBox:{
          marginLeft:20,
          marginRight:10,
          height:30,
          backgroundColor:'#fff',
          alignSelf:'stretch',
          shadowColor:'#000',
          marginTop:10

      },
      card:{
        flex:1,
        flexDirection:"column",
        width:'95%',
        height:70,
        marginTop:10,
        padding:1, 
        marginLeft:10,
        marginRight:10 ,
        borderRadius:10,
        backgroundColor:'white' 
    },
    uppDiv:{
        flex:1,
        flexDirection:"row",
        justifyContent:'space-between'
        

        
    },
    belowDiv:{
        flex:1,
        flexDirection:"row",
        justifyContent:"space-between"
  
    }
    



})

const maptToProps =dispatch =>{
    return{
        reciveCheque:(data)=>dispatch(AddChequeReciveFun(data)),
        ChequeReciveDatafun:(data)=>dispatch(ChequeReciveDatafun(data)),
        DeleteReciveDatafun:(data)=>dispatch(DeleteReciveDatafun(data)),
        UpdateReciveChequefun:(data)=>dispatch(UpdateReciveChequefun(data)),
        UpdateReciveChequestatusfun:(data)=>dispatch(UpdateReciveChequestatusfun(data))
        
    }
}

const mapToState =state =>{
    return{
      data:state.auth
    }
  }


export default connect(mapToState,maptToProps)(ReceiveCheque)