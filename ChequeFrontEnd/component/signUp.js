import React, { Component } from 'react';
import { View, Text, StyleSheet,TextInput } from 'react-native';
import { Button } from 'react-native-elements';


class SignUp extends Component {
    static navigationOptions = {
        title: 'CreateAccount',
        headerStyle: { backgroundColor: 'rgb(148, 20, 103)' },
        headerTitleStyle: { color: 'white' },
        headerTintColor: 'white',
      };
      
    render() {
        return (
            <View style={styles.container}>
                <TextInput style={styles.InputFiled}></TextInput>
                <TextInput style={styles.InputFiled}></TextInput>
                <TextInput style={styles.InputFiled}></TextInput>
                <TextInput style={styles.InputFiled}></TextInput>
                <View ><Button title="signUp" color={'rgb(148, 20, 103)'}></Button></View>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
       
    },
    InputFiled:{
        alignItems:'center',
        marginLeft:5,
        marginRight:5,
        marginBottom:10,
        borderWidth:1,
        borderRadius:5,
        borderColor: '#C0C0C0',
        backgroundColor:'#fff',
        alignSelf:'stretch',
        shadowColor:'#000',
    },
    ButtonStyle:{
        width:300,
            
    },
    TextStyle:{
        alignSelf:'center',
        color:"#007aff",
        fontSize:18,
        fontWeight:'600',
        paddingTop:10,
        paddingBottom:10,
    }
});


export default SignUp;
