import React,{Component} from 'react'
import {View,StyleSheet,Text,Button,Dimensions,AsyncStorage,TouchableOpacity,
  Picker,AppState,I18nManager} from 'react-native'
import { Calendar, CalendarList, Agenda ,calendarTheme} from 'react-native-calendars';
import {connect} from 'react-redux'
import {ChequeReciveDatafun,GetALlChequesCreate} from '../actions/actions'
import { NavigationEvents } from "react-navigation";
import {strings} from './Strings'
import NotifService from '../notification/NotifService '
import PushNotification from 'react-native-push-notification'
import PushController from '../notification/PushNotification';
import RNRestart from 'react-native-restart';










class Home extends Component{

  constructor(props){
    super(props)
    this.handleAppStateChange = this.handleAppStateChange.bind(this);
    this.state={
      data:'',
      user_id:'',
      dates:{},
      description:{},
      number:1,
      datesNotRecive:[],
      datesNotCreate:[],
      go:[{value:'good'},{value:'good'},{value:'good'},{value:'good'}],
      test:[{Date:'2019-04-18'}],
      LastCheqesId:''

    }
   // this.notif = new NotifService(this.onRegister.bind(), this.onNotif.bind(this));
  }


  componentDidMount(){
    AppState.addEventListener('change', this.handleAppStateChange);            
    AsyncStorage.getItem('userId').then((user_id)=>{
 
      this.setState({user_id},()=>{
        this.handlerRefresh()
           
      }) 
    
  })

  } 

componentWillMount(){
  AsyncStorage.getItem("language").then((language)=>{
    if(language=='ar'){
      I18nManager.forceRTL(true)
      if(I18nManager.isRTL){
        RNRestart.Restart();
      }

    }
    else{
      I18nManager.forceRTL(false)
    }
  })

  AppState.removeEventListener('change', this.handleAppStateChange);
}
handlerRefresh=()=>{
  
  this.setState({dates:{}})
  this.setState({datesNotRecive:[]})
  this.setState({description:{}})
  const today=new Date()
  var dd=String(today.getDate()).padStart(2,'0')
  var mm=String(today.getMonth()+1).padStart(2,'0')
  var yy=String(today.getFullYear())
  var date=yy+'-'+mm+'-'+dd
  setTimeout(() => {
    


  //get all recive cheques 
  this.props.GetALlChequesCreate(this.state.user_id).then(res=>{
    this.setState({data:Array.prototype.reverse.call(this.props.data.data)})

    
    this.state.data.map((item)=>{

    if (item.date<date && item.status==0){
      this.setState((prev)=>({
        dates:{...prev.dates ,[item.date]:{selected: true, marked: true,selectedColor:'gray'}}
      }))

      if(this.state.description[item.date]) {
        this.state.description[item.date].push({amount:strings.ChequesCalender+'\n\n'+item.for_whom+'\n\n'+item.bank_name+'\n\n'+item.value +' '+item.coin_name})        
      }
      else{
        this.setState((prev)=>({
          description:{...prev.description,[item.date]:[{amount:strings.ChequesCalender+'\n\n'+item.for_whom+'\n\n'+item.bank_name+'\n\n'+item.value +' '+item.coin_name}]}
        }))
      }

      this.state.datesNotRecive.push({Date:item.date,Value:item.value,Bank_name:item.bank_name})
    }
    
   else if (item.date>date && item.status==0){
      this.setState((prev)=>({
        dates:{...prev.dates ,[item.date]:{selected: true, marked: true,selectedColor:'orange'}},
      })) 

      if(this.state.description[item.date]) {
          this.state.description[item.date].push({amount:strings.ChequesCalender+'\n\n'+item.for_whom+'\n\n'+item.bank_name+'\n\n'+item.value +' '+item.coin_name})        
      }
      else{
        this.setState((prev)=>({
          description:{...prev.description,[item.date]:[{amount:strings.ChequesCalender+'\n\n'+item.for_whom+'\n\n'+item.bank_name+'\n\n'+item.value +' '+item.coin_name}]}
        }))
      }
      this.state.datesNotRecive.push({Date:item.date,Value:item.value,Bank_name:item.bank_name})

    }

    else if (item.status ==1){
      this.setState((prev)=>({
        dates:{...prev.dates ,[item.date]:{selected: true, marked: true,selectedColor:'red'}},
      })) 

      if(this.state.description[item.date]) {
        this.state.description[item.date].push({amount:strings.ChequesCalender+'\n\n'+item.for_whom+'\n\n'+item.bank_name+'\n\n'+item.value +' '+item.coin_name})        
    }
    else{
      this.setState((prev)=>({
        description:{...prev.description,[item.date]:[{amount:strings.ChequesCalender+'\n\n'+item.for_whom+'\n\n'+item.bank_name+'\n\n'+item.value +' '+item.coin_name}]}
      }))
    }
    this.state.datesNotRecive.push({Date:item.date,Value:item.value,Bank_name:item.bank_name})
    }
  
    })
  })

  this.props.ChequeReciveDatafun({user_id:this.state.user_id}).then(res=>{
    this.setState({data:Array.prototype.reverse.call(this.props.data.data)})

    
    this.state.data.map((item , id)=>{
    if (item.date<=date && item.status==0){
      this.setState((prev)=>({
        dates:{...prev.dates ,[item.date] :{selected: true, marked: true,selectedColor:'gray'}},
        //datesNotRecive:{...prev.datesNotRecive,[item.date]:{Value:item.value,Bank_name:item.bank_name}}
      })) 
      if(this.state.description[item.date]){
        this.state.description[item.date].push({amount:strings.RecivedCalender+'\n\n'+item.from_who+'\n\n'+item.bank_name+'\n\n'+item.value +' '+item.coin})
      }
      else{
        this.setState((prev)=>({
          description:{...prev.description,[item.date]:[{amount:strings.RecivedCalender+'\n\n'+item.from_who+'\n\n'+item.bank_name+'\n\n'+item.value +' '+item.coin}]},
        }))
      }
      this.state.datesNotRecive.push({Date:item.date,Value:item.value,Bank_name:item.bank_name})
    }
   else if (item.date>date && item.status==0){
      this.setState((prev)=>({
        dates:{...prev.dates ,[item.date]:{selected: true, marked: true,selectedColor:'orange'}},
        //datesNotRecive:{...prev.datesNotRecive,[item.date]:{Value:item.value,Bank_name:item.bank_name}}
      })) 
      if(this.state.description[item.date]){
        this.state.description[item.date].push({amount:strings.RecivedCalender+'\n\n'+item.from_who+'\n\n'+item.bank_name+'\n\n'+item.value +' '+item.coin})
      }
      else{
        this.setState((prev)=>({
          description:{...prev.description,[item.date]:[{amount:strings.RecivedCalender+'\n\n'+item.from_who+'\n\n'+item.bank_name+'\n\n'+item.value +' '+item.coin}]},
        }))
      }
      this.state.datesNotRecive.push({Date:item.date,Value:item.value,Bank_name:item.bank_name})
    }
    else if (item.status ==1){
      this.setState((prev)=>({
        dates:{...prev.dates ,[item.date]:{selected: true, marked: true,selectedColor:'red'}},
        //datesNotRecive:{...prev.datesNotRecive,[item.date]:{Value:item.value,Bank_name:item.bank_name}}s
      })) 
      if(this.state.description[item.date]){
        this.state.description[item.date].push({amount:strings.RecivedCalender+'\n\n'+item.from_who+'\n\n'+item.bank_name+'\n\n'+item.value +' '+item.coin})
      }
      else{
        this.setState((prev)=>({
          description:{...prev.description,[item.date]:[{amount:strings.RecivedCalender+'\n\n'+item.from_who+'\n\n'+item.bank_name+'\n\n'+item.value +' '+item.coin}]},
        }))
      }
      this.state.datesNotRecive.push({Date:item.date,Value:item.value,Bank_name:item.bank_name})
    }
  
    })
  })

}, 50);

}
handlerChangeNumber=()=>{
  this.setState({number:2})
}


handleAppStateChange(appState) {
  if (appState === 'active') {
    PushNotification.cancelAllLocalNotifications()
    //console.log("welcome")
  }
  if (appState === 'background') {
    let date = new Date(Date.now());
    let  dd =String(date.getDate()).padStart(2, '0');
    let  mm = String(date.getMonth() + 1).padStart(2, '0'); 
    let  y = date.getFullYear();
    let datee=y+ '-' + mm + '-' + dd
    var nextWeek = new Date(date.getTime() - 7 * 24 * 60 * 60 * 1000);
    //console.log(datee)
    //console.log(datee)
    //console.log(nextWeek)
    
    // date.setHours(10,52,0)
    //console.log(date) 
    //console.log(this.state.datesNotRecive)
 
    //console.log(this.state.datesNotRecive)

    this.state.datesNotRecive.map((item)=>{
      ODate=new Date(item.Date)
      if (item.Date>datee){
        TodayItemAt8 =new Date(ODate)
        TodayItemAt8.setHours(9,0,0)
        TodayItemAt2=new Date(ODate)
        TodayItemAt8.setHours(14,0,0)

        PushNotification.localNotificationSchedule({
          title:strings.titleApp,
          message:strings.TodaysChequeNotification+item.Value,
          subText:item.Bank_name,
          date:TodayItemAt8
        })
        PushNotification.localNotificationSchedule({
          title:strings.titleApp,
          message:strings.TodaysChequeNotification+item.Value,
          subText:item.Bank_name,
          date:TodayItemAt2
        })
        //console.log(TodayItemAt8,TodayItemAt2 ,"wwwww")
      } 
      if (new Date(ODate.getTime() - 7 * 24 * 60 * 60 * 1000) > date ){
        nextWeek= new Date(ODate.getTime() - 7 * 24 * 60 * 60 * 1000)
        nextWeek.setHours(12 , 30 ,0)
        PushNotification.localNotificationSchedule({
          title:strings.titleApp,
          message:strings.weekChequeNotification+item.Value,
          subText:item.Bank_name,
          date:nextWeek
        })
       // console.log(nextWeek,"aaaaaaaaa")
      }
      if (new Date(ODate.getTime() - 3 * 24 * 60 * 60 * 1000) > date){
        nextThree= new Date(ODate.getTime() - 3 * 24 * 60 * 60 * 1000)
        nextThree.setHours(12, 0, 0)
        PushNotification.localNotificationSchedule({
          title:strings.titleApp,
          message:strings.ThreeDaysChequeNotification +item.Value,
          subText:item.Bank_name,
          date:nextThree
        })
        //console.log(nextThree,"bbbbbbbbbbbbb")
      }
    })

   
      //console.log(this.state.datesNotRecive)
    
      

  
  
  }
}


render(){

renderEmptyDate=()=> {
  return (
    <View style={styles.emptyDate}><Text>This is empty date!</Text></View>
  );
}

return(
    <View style={styles.contianer}>
        {/*
        
      
          <NavigationEvents
          onWillFocus={payload => {
            {this.state.number !=1 ?
            this.handlerRefresh()
            
            : this.handlerChangeNumber()}
          }}
        />
        */}

    <Agenda
    items={
      [this.state.data.date]
    }
     items={this.state.description}
       

        
        
    markedDates={this.state.dates}
    
          renderItem={(item, firstItemInDay) => {return (<View style={[styles.item, {justifyContent:'center'}]}><Text style={{fontSize:15,fontWeight:'bold'}}> {item.amount}</Text></View>);}}
          // specify how each date should be rendered. day can be undefined if the item is not first in that day.
          //renderDay={(day, item) => {if (day){return (<View><Text>{day.dateString}</Text></View>);}}}
          // specify how empty date content with no items should be rendered
          renderEmptyDate={() => {return (<View style={styles.emptyDate}><Text>This is empty date!</Text></View>);}}
          // specify how agenda knob should look like
          renderKnob={() => {return (<View />);}}
          // specify what should be rendered instead of ActivityIndicator
          renderEmptyData = {() => {return (<View />);}}
          // specify your item comparison function for increased performance
          rowHasChanged={(r1, r2) => {return r1.text !== r2.text}}

        theme={{
            ...calendarTheme,
            agendaDayTextColor: 'gray',
            agendaDayNumColor: 'gray',
            agendaTodayColor: 'red',
            agendaKnobColor: 'blue'
          }}
          // agenda container style
          style={{}}
          

          onRefresh={() =>this.handlerRefresh()}
          // Set this true while waiting for new data from a refresh
          refreshing={false}
          // Add a custom RefreshControl component, used to provide pull-to-refresh functionality for the ScrollView.
          refreshControl={null}
          
       

    />

<PushController />      
    </View>

   

        
    
)
}
}
const styles=StyleSheet.create({
    contianer:{
        flex:1
    },
    item: {
        backgroundColor: 'white',
        flex: 1,
        borderRadius: 5,
        padding: 10,
        marginRight: 10,
        marginTop: 17
      },
      emptyDate: {
        height: 15,
        flex:1,
        paddingTop: 30
      }
})

const mapToDispatch=dispatch=>{
  return{
    GetALlChequesCreate:(data)=>dispatch(GetALlChequesCreate(data)),
    ChequeReciveDatafun:(data)=>dispatch(ChequeReciveDatafun(data)),
  }

}
const mapTostate=state=>{
  return{
    data:state.auth
  }
}
export default  connect(mapTostate,mapToDispatch)(Home)