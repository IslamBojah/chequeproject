
import React,{Component} from 'react'
import {StyleSheet,View, Text,TouchableOpacity,TouchableWithoutFeedback,ActivityIndicator,
    DatePickerAndroid,TextInput,CheckBox,ScrollView , FlatList , Image,Alert} from 'react-native'
import {connect} from 'react-redux'
import {GetAllBooksChequesFun,addChequeFun,editChequeFun,deletechequeFun,
    updateChequeStatusFun,UpdateBookAvailableSizeFun} from '../actions/actions'
import Icon from 'react-native-vector-icons/FontAwesome';
import Modal from 'react-native-modal'
import {strings} from './Strings'


const today=new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();


class BookCheques extends Component {


    state={
        book_id:'',

        chequesData:'',
        isModalVisible:false,

        simpleDate: new Date(Date.now()),
        simpleText:yyyy + '-' + mm + '-' + dd,
        bankStyle:styles.InputFiled,
        coinStyle:styles.InputFiled,
        pickerDateStyle:styles.InputFiled,
        chequeValueStyle:styles.InputFiled,
        namechequeRecStyle:styles.InputFiled,
        chequeNumberStyle:styles.InputFiled,

        chequeStautsStyle:styles.CheckBox,

        

        bank:'',
        coin:'',


        checkedPayee:false,
        checkedCrossed:false,
        chequeValue:'',
        chequeNameofRec:'',
        chequeNum:'',
        chequeDate:yyyy + '-' + mm + '-' + dd,
        cheque_id:'',
        loading:false,
        isEdit:false,

        isPrimary:'',
        isCrossed:'',
        editable:false,
        editId:'',
        bookSize:'' ,
        available_size:''

    }

    showPicker = async (stateKey, options) => {
        try {
          var newState = {};
          const {action, year, month, day} = await DatePickerAndroid.open(options);
          if (action === DatePickerAndroid.dismissedAction) {
            newState[stateKey + 'Text'] = this.state.simpleText;
          } else {
            var date = new Date(year, month, day )
            
            newState[stateKey + 'Text'] = date.toLocaleDateString()
            newState[stateKey + 'Date'] = date;
          }
          this.setState(newState);
          var d = String(date.getDate()).padStart(2, '0');
          var m = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
          var y = date.getFullYear();

          this.setState({chequeDate:y+ '-' + m + '-' +d})
          this.setState({simpleText:this.state.chequeDate})
        } catch ({code, message}) {
          console.warn(`Error in example '${stateKey}': `, message);
        }
      };
    componentDidMount(){
        const book_id = this.props.navigation.getParam('book_id', '');
        const coin=this.props.navigation.getParam('currency','')
        const bank=this.props.navigation.getParam('bankName','')
        const bookSize=this.props.navigation.getParam('bookSize','')
        const available_size=this.props.navigation.getParam('available_size' , '');

       
    // alert(available_size)
        this.setState({available_size})
        this.setState({bookSize})
        this.setState({coin})
        this.setState({book_id})
        this.setState({bank})
        
        this.props.GetAllBooksCheques({book_id:book_id})
        .then(res=>{
            if(Object.keys(this.props.data.data).length===0){
                this.setState({chequesData:''})
            }
            else{
                this.setState({chequesData:Array.prototype.reverse.call(this.props.data.data)})
            }
        })
    }

    _toggleModal=()=>{
        this.setState({isModalVisible:!this.state.isModalVisible})
        this.setState({pickerDateStyle:styles.InputFiled})
        this.setState({chequeValueStyle:styles.InputFiled})
        this.setState({namechequeRecStyle:[{padding:10},styles.InputFiled]})
        this.setState({chequeNumberStyle:[{padding:10},styles.InputFiled]})
        this.setState({checkedPayee:false})
        this.setState({checkedCrossed:false})
        this.setState({chequeValue:''})
        this.setState({chequeNameofRec:''})
        this.setState({chequeNum:''})
        this.setState({chequeDate:dd + '-' + mm + '-' + yyyy})
        this.setState({simpleText:this.state.chequeDate})    
        this.setState({isEdit:false})  
        this.setState({editable:false})
        this.setState({editId:''})
        
    }

    Tochablelogin=()=>{
      //  alert(this.state.available_size)
        if(parseInt (this.state.available_size) <= 0){
            alert('you access the size of book')
        }
        else{
        this.setState({isModalVisible:!this.state.isModalVisible})
        this.setState({pickerDateStyle:styles.InputFiled})
        this.setState({chequeValueStyle:styles.InputFiled})
        this.setState({namechequeRecStyle:[{padding:10},styles.InputFiled]})
        this.setState({chequeNumberStyle:[{padding:10},styles.InputFiled]})
        this.setState({checkedPayee:false})
        this.setState({checkedCrossed:false})
        this.setState({chequeValue:''})
        this.setState({chequeNameofRec:''})
        this.setState({chequeNum:''})
        this.setState({chequeDate:dd + '-' + mm + '-' + yyyy})
        this.setState({simpleText:this.state.chequeDate})    
        this.setState({isEdit:false})  
        this.setState({editable:false})
        this.setState({editId:''})
        }  
    }

    onValueChange=(value)=>{
        if (value >0 ){
            this.setState({chequeValue:value}) 
            this.setState({chequeValueStyle:[{padding:10},styles.InputFiled]})
        }
        else{
            this.setState({chequeValue:value}) 
            this.setState({chequeValueStyle:[{padding:10},styles.InputFiledError]})
        }
    }
    onNamwRecipientChange=(name)=>{

        this.setState({chequeNameofRec:name},
            ()=>{
                if(!(/[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z ]+[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z-_ ]{3,}$/).test(this.state.chequeNameofRec)){
                    this.setState({namechequeRecStyle:[{padding:10},styles.InputFiledError]})
                }
                else{
                    this.setState({namechequeRecStyle:[{padding:10},styles.InputFiled]})   
                }
            })
    }

    chequeNumChange=(num)=>{
        this.setState({chequeNum:num})
        this.setState({chequeNumberStyle:[{padding:10},styles.InputFiled]})
    }

    _CreateNewCheque=()=>{
        if(!this.state.chequeDate || !this.state.chequeValue || !this.state.chequeNameofRec ||!this.state.chequeNum){
            if(!this.state.chequeDate){
                this.setState({pickerDateStyle:styles.InputFiledError})
            }
            else{
                this.setState({pickerDateStyle:styles.InputFiled})
            }
            if(!this.state.chequeValue || this.state.chequeValue<=0 ){
                
                this.setState({chequeValueStyle:styles.InputFiledError})
            }
            else{
                this.setState({chequeValueStyle:styles.InputFiled})
            }
            if(!this.state.chequeNameofRec || !(/[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z ]+[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z-_ ]{3,}$/).test(this.state.chequeNameofRec)){
                this.setState({namechequeRecStyle:[{padding:10},styles.InputFiledError]})
            }
            else{
                this.setState({namechequeRecStyle:[{padding:10},styles.InputFiled]})
            }
            if (!this.state.chequeNum){
                this.setState({chequeNumberStyle:[{padding:10},styles.InputFiledError]})
            }
            else{
                this.setState({chequeNumberStyle:[{padding:10},styles.InputFiled]}) 
            }
        }
        else if(!this.state.chequeValue || isNaN(this.state.chequeValue) || this.state.chequeValue<=0 ){
            this.setState({chequeValueStyle:styles.InputFiledError})
        }
        else if(!this.state.chequeNameofRec || !(/[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z ]+[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z-_ ]{3,}$/).test(this.state.chequeNameofRec) ){
            this.setState({namechequeRecStyle:[{padding:10},styles.InputFiledError]})
        }
 
        else{

            if(this.state.checkedPayee==false){
                this.setState({isPrimary:'0'})
            }
            else{
                this.setState({isPrimary:'1'})
            }

            if(this.state.checkedCrossed==false){
                this.setState({isCrossed:'0'})
            }
            else{
                this.setState({isCrossed:'1'})
            }

            if(this.state.isEdit==false){

            //    alert(this.state.isCrossed)
            obj={value:this.state.chequeValue , date:this.state.chequeDate , book_id:this.state.book_id,
            for_whom:this.state.chequeNameofRec , cheque_no:this.state.chequeNum , is_primary:this.state.isPrimary,
            is_crossed:this.state.isCrossed
            }

            this.props.addCheque(obj).then(res=>{
                if(this.state.chequesData==''){
                    this.setState({chequesData:[this.props.data.data[0] ]}) 
                }
                else{
                    this.state.chequesData.unshift(this.props.data.data[0])
                }
                this.setState({available_size :parseInt(this.state.available_size)-1})
                setTimeout(function(){
                    this.props.UpdateAvailableSize({book_id:this.state.book_id,available_size:this.state.available_size})
                }.bind(this),1000)
            })
     
            }
            else{
                
                obj={cheque_id:this.state.cheque_id , value:this.state.chequeValue , date:this.state.chequeDate , 
                    for_whom:this.state.chequeNameofRec , cheque_no:this.state.chequeNum , is_primary:this.state.isPrimary,
                    is_crossed:this.state.isCrossed
                    }
                    this.props.editChequeFun(obj).then(
                        this.state.chequesData.map((user)=>{
                            if(user.cheque_id===this.state.cheque_id){
                                user.value=this.state.chequeValue 
                                user.date=this.state.chequeDate 
                                user.for_whom=this.state.chequeNameofRec 
                                user.cheque_no=this.state.chequeNum
                                user.is_primary=this.state.isPrimary
                                user.is_crossed=this.state.isCrossed
                                
                            }
                        })
                    )
             
            }
            this.setState({loading:true})
            setTimeout(function(){
                this._toggleModal()
                this.setState({loading:false})
            }.bind(this),1000)
        }
    }

    _onPress=(rowData)=>{
        this.setState({isEdit:true})
        this.setState({isModalVisible:!this.state.isModalVisible})
        this.setState({pickerDateStyle:styles.InputFiled})
        this.setState({chequeValueStyle:styles.InputFiled})
        this.setState({namechequeRecStyle:[{padding:10},styles.InputFiled]})
        this.setState({chequeNumberStyle:[{padding:10},styles.InputFiled]})
        this.setState({cheque_id:rowData.cheque_id})
        if(rowData.is_primary==0){
            this.setState({checkedPayee:false})
        }
        else{
            this.setState({checkedPayee:true})
        }
        if(rowData.is_crossed==0){
            this.setState({checkedCrossed:false})
        }
        else{
            this.setState({checkedCrossed:true})
        }

        this.setState({chequeValue:rowData.value})
        this.setState({chequeNameofRec:rowData.for_whom})
        this.setState({chequeNum:rowData.cheque_no})
        this.setState({chequeDate:rowData.date})
        this.setState({simpleText:rowData.date}) 
        
        const today=new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();   
        
        var date= yyyy+'-'+mm+'-'+dd

        if(date >= rowData.date){
            this.setState({editable:true})
        }
        else{
            this.setState({editable:false})
        }
        this.setState({editId:rowData.cheque_id})
         
    }

    _onLongPress=(rowData)=>{    
       
        Alert.alert(
         strings.DeleteItem,
         strings.ConfirmDeleteItem,
         [
          {
             text: strings.cancel,
             onPress: () => console.log('Cancel Pressed'),
             style: 'cancel',
          },
         {text:strings.DeleteItem, onPress: () => Alert.alert(
            strings.ConfirmDeleteItem,
             strings.DeleteItem,
             [
                 {
                     text:strings.ok ,onPress:()=>{
                       this.props.deletechequeFun(rowData.cheque_id)
                       const newChequeData = this.state.chequesData.filter((user)=>{
                        return user.cheque_id != rowData.cheque_id
                        })
                    this.setState({chequesData:newChequeData})

                   // setTimeout(function(){
                    //    this.props.UpdateAvailableSize({book_id:this.state.book_id,available_size:available_size})
                    //}.bind(this),1000)


                     }
                 },
                 {
                    text:strings.cancel 
                 }
             ]
           )
         },
         ],
         {cancelable: true},
        )       
     }

     handlerDoneCheque=()=>{
        var data={status:'2' , id:this.state.cheque_id}
        this.props.updateChequstatus(data).then(res=>{
            this.state.chequesData.map((checks)=>{
                if(checks.cheque_id ==this.state.cheque_id){
                    checks.status='2'
                }
            })
            this.setState({isModalVisible:false})
            this.setState({editable:false})
            this._toggleModal
        })
    }
    handlerBoundedCheque=()=>{
        var data={status:'1' , id:this.state.cheque_id}
        this.props.updateChequstatus(data).then(res=>{
            this.state.chequesData.map((checkes)=>{
               if(checkes.cheque_id ==this.state.cheque_id){
                    checkes.status='1'
                }
              })
              this.setState({isModalVisible:false})
              this.setState({editable:false})
            this._toggleModal

        })
    }
    render(){
        return(
            <View style={styles.contianer}>
                {this.state.chequesData !='' &&
                    <FlatList
                    data={this.state.chequesData}
                    extraData={this.state}
                    style={{marginBottom:10}}
                    renderItem={({item:rowData})=>{
                        return(
                            <TouchableOpacity onPress={rowData.status=='2'? null :this._onPress.bind(this,rowData)} onLongPress={this._onLongPress.bind(this,rowData)}>

                            <View style={rowData.status=='1' ?[styles.card,{borderWidth:1,borderColor:'#d50000'}] :rowData.status=='2' ?[styles.card,{borderWidth:1,borderColor:'green'}]:styles.card} >

                                <View style={styles.upperDiv}>
                                    <Text  style={{fontSize:16,fontWeight:'bold',}} >{rowData.for_whom}</Text>
                                    <Text style={{fontSize:16,fontWeight:'bold',fontStyle: 'italic',}}>{rowData.date}</Text>
                                </View>
                                <View style={styles.lowerDiv}>
                                    <Text  style={{ textAlignVertical:'center'}} >{this.state.bank}</Text>
                                    {rowData.status=='1' &&
                                        <Text style={{color:'#d50000' , fontWeight:'bold'}}>Bounded</Text>
                                    }
                                    {rowData.status=='2' &&
                                        <Text style={{color:'green' , fontWeight:'bold'}}>Done</Text>
                                    }
                                    <Text style={{ textAlignVertical:'center', color:'#d50000'}}>{rowData.value} {rowData.coin}</Text>
                                </View>
                            </View>
              
                        </TouchableOpacity>
                        )

                    }}
                    keyExtractor={(item,index)=>index.toString()}
                    >
                        
                    </FlatList>
                
                }

                <TouchableOpacity style={styles.BottoShap} onPress={this.Tochablelogin}>
                    <Icon name={"plus"}  size={30} color="#fff" />
                </TouchableOpacity>

    <Modal
        isVisible={this.state.isModalVisible}
        onBackButtonPress={this._toggleModal}
        onBackdropPress={()=>null}
        >
    
        <ScrollView contentContainerStyle={styles.contentContainer} >
                
        <View style={styles.ModalStyle}>
            <Text style={{marginTop:10 ,color:'black' , fontSize:20 ,fontWeight:'600',marginBottom:15,alignSelf:'center' }}>{strings.createCheque}</Text>
                
            <TouchableWithoutFeedback
                disabled={this.state.editable==true ? true:false}
                onPress={this.showPicker.bind(this, 'simple',{date: this.state.simpleDate})} value={this.state.simpleText}>
                <View style={this.state.pickerDateStyle}><Text style={{fontSize:15,padding:10}}>{this.state.simpleText}</Text></View> 
            </TouchableWithoutFeedback>
          
            <View style={{ flexDirection:'row'}}> 
                <View style={{flex:2}}>
                    <TextInput keyboardType='number-pad'
                    editable={this.state.editable==true ? false :true}  
                    onChangeText={this.onValueChange} placeholder={strings.ValueChequeBook} 
                     style={[{padding:10},this.state.chequeValueStyle]} 
                     value={this.state.chequeValue} />
                </View>
                <View style={[{flex:1,padding:10},styles.InputFiled]}  >
                <Text style={{color:'#000'}}>{this.state.coin}</Text>
                </View>

            </View>

        
          <TextInput onChangeText={this.onNamwRecipientChange} 
          placeholder={strings.nameRecipientChecqueBook} 
          style={[{padding:10},this.state.namechequeRecStyle]} 
          value={this.state.chequeNameofRec} 
          maxLength={50}
          editable={this.state.editable==true ? false :true} 
          />

          <TextInput onChangeText={this.chequeNumChange} 
          editable={this.state.editable==true ? false :true} 
          keyboardType='numeric' placeholder={strings.chequeNumChequeBook} style={[{padding:10},this.state.chequeNumberStyle]} value={this.state.chequeNum} />
         
          <View style={[{flex:1,padding:10},styles.InputFiled]}>
            <Text style={{color:'#000'}}>{this.state.bank}</Text>
          </View>

          <View style={[{flex:1,flexDirection:'row'},this.state.chequeStautsStyle]}>
            <CheckBox
                value={this.state.checkedPayee}
                onValueChange={() => this.setState({ checkedPayee: !this.state.checkedPayee })}
                disabled={this.state.editable==true?true:false}
            />
            <Text style={{marginTop: 5}}>{strings.PaidForPrimaryCheque}</Text>
          </View>

          <View style={[{flex:1,flexDirection:'row'},this.state.chequeStautsStyle]}>
            <CheckBox
                value={this.state.checkedCrossed}
                onValueChange={() => this.setState({ checkedCrossed: !this.state.checkedCrossed })}
                disabled={this.state.editable==true?true:false}
            />
            <Text style={{marginTop: 5}}>{strings.crossedCheque}</Text>
          </View>

            {this.state.loading===false ? 
                <View>
                    {this.state.isEdit===false  &&
                        <View style={{flexDirection:'row' ,justifyContent:'space-evenly',alignSelf:'stretch'}}>
                            <TouchableOpacity onPress={this._toggleModal} 
                            style={styles.TouchableOpacityStyle} >
                            <Text style={styles.ButtonStyle}>{strings.cancel}</Text>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={this._CreateNewCheque} 
                            style={styles.TouchableOpacityStyle} >
                            <Text style={styles.ButtonStyle}>{strings.Create}</Text>
                            </TouchableOpacity>
                        </View>

                    } 

               

                {(this.state.isEdit===true && this.state.editable==false) &&

                    <View style={{flexDirection:'row' ,justifyContent:'space-evenly',alignSelf:'stretch'}}>
                        <TouchableOpacity onPress={this._toggleModal} 
                        style={styles.TouchableOpacityStyle} >
                        <Text style={styles.ButtonStyle}>{strings.cancel}</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={this._CreateNewCheque} 
                        style={styles.TouchableOpacityStyle} >
                        <Text style={styles.ButtonStyle}>{strings.EditItem}</Text>
                        </TouchableOpacity>
                    </View>
                
                } 

                {(this.state.isEdit===true && this.state.editable==true) &&
                    <View style={{flexDirection:'row' ,justifyContent:'space-evenly',alignSelf:'stretch'}}>
                        <TouchableOpacity onPress={this.handlerBoundedCheque} 
                        style={styles.TouchableOpacityStyle} >
                        <Text style={styles.ButtonStyle}>{strings.Bounded}</Text>
                        </TouchableOpacity>
    
                        <TouchableOpacity onPress={this.handlerDoneCheque} 
                        style={styles.TouchableOpacityStyle} >
                        <Text style={styles.ButtonStyle}>{strings.Done}</Text>
                        </TouchableOpacity>
                    </View>
                }
            
             </View> 

             :
             <ActivityIndicator size='large' color="#007aff"/>

            }
            </View>

                  
            
        

                
        

         </ScrollView>
        
        </Modal>

            </View>
        )
    }
}

const styles=StyleSheet.create({
    contianer:{
        flex:1,
        flexDirection:"column",
    },

    homeDiv:{
        flex:1,
        justifyContent:'flex-end',
       
    },
    
    BottoShap:{
        borderWidth:2,
        alignItems:'center',
        justifyContent:'center',
        width:50,
        height:50,
        backgroundColor:'rgb(148,20,103)',
        borderRadius:50,
        position:'absolute', 
        alignSelf:'flex-end',
        zIndex: 3 ,
        bottom: '5%',
        right:'5%',
        borderColor:'rgb(148,20,103)'

    },
    button: {
        borderWidth: 1,
        borderColor: "#000000",
        margin: 5,
        padding: 5,
        width: "70%",
        backgroundColor: "#DDDDDD",
        borderRadius: 5,
      },
      ModalStyle:{ 
        backgroundColor:'white',
        padding: 10,
        borderRadius:5,
        alignSelf:'stretch'
      },
      TouchableOpacityStyle:{
        backgroundColor:"#941467",
        width:100 ,
        height:40 ,
        alignItems: 'center',
        justifyContent:'center',
        marginTop:20,
        marginBottom:20,
        borderRadius:10
    },
    ButtonStyle:{
        color:'#FFF',
        fontSize:17,
        fontWeight:'500'
    },
    InputFiled:{
        marginLeft:20,
        marginRight:10,
        height:40,
        borderColor:'#c0c0c0',
        borderWidth:1,
        borderRadius:5,
        backgroundColor:'#fff',
        alignSelf:'stretch',
        shadowColor:'#000',
        marginTop:10,  
             
    },
    CheckBox:{
        marginLeft:20,
        marginRight:10,
        height:30,
        backgroundColor:'#fff',
        alignSelf:'stretch',
        shadowColor:'#000',
        marginTop:10,      
    },
    InputFiledError:{
        marginLeft:20,
        marginRight:10,
        borderColor:'#d50000',
        borderWidth:1,
        borderRadius:5,
        backgroundColor:'#fff',
        alignSelf:'stretch',
        shadowColor:'#000',
        marginTop:10,
        height:40
    },

    contentContainer: {
        paddingVertical: 20
      },
      card:{
        flex:1,
        flexDirection: 'column',
        width:'95%',
        height:70,
        marginTop:10,
        padding:1,
        marginLeft:10,
        marginRight:10 ,
        borderRadius:10,
        backgroundColor:'white' ,
        padding:10
    },
    upperDiv:{
        flex:1,
        flexDirection:'row',
        justifyContent:'space-between'
    },
    lowerDiv:{
        flex:1,
        flexDirection:'row',
        justifyContent:'space-between'
    }
})
const mapToProps = dispatch =>{
    return{
        GetAllBooksCheques:(data)=>dispatch(GetAllBooksChequesFun(data)),
        addCheque:(data)=>dispatch(addChequeFun(data)),
        editChequeFun:(data)=>dispatch(editChequeFun(data)),
        deletechequeFun:(data)=>dispatch(deletechequeFun(data)),
        updateChequstatus:(data)=>dispatch(updateChequeStatusFun(data)),
        UpdateAvailableSize:(data)=>dispatch(UpdateBookAvailableSizeFun(data))
       
    }

  }

const mapToState =state =>{
    return{
      data:state.auth
    }
  }

export default connect(mapToState,mapToProps)(BookCheques)