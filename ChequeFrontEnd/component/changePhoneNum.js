import React,{Component} from 'react'
import {View,StyleSheet,KeyboardAvoidingView,TextInput,TouchableOpacity,ActivityIndicator
    ,Text,AsyncStorage,Image,Picker} from 'react-native'
import {strings} from './Strings'
import HanderError from './inlineError'
import {updateUserPhoneFun} from '../actions/actions'
import  {connect} from 'react-redux'
import Modal from 'react-native-modal'
import succ from '../Images/checked.png'

class changePhoneNum extends Component{
    state={
        loading:true,
        oldPhone:'',
        newPhone:'',
        IntroductionOld:'970',
        Introduction:'970',

        oldPhoneStyle:styles.mobileDiv,
        newPhoneStyle:styles.mobileDiv,
        
        Errors:{},
        userId:'',
        isModalVisible:false
    }

    componentDidMount(){
        AsyncStorage.getItem('userId').then((userId)=>{
            this.setState({userId})
        })
    }
    _renderButton(){
        if(this.state.loading){
        return (
            <View style={[styles.ElementDiv,{flexDirection:'row' ,justifyContent:'space-between',alignSelf:'stretch'}]}>
            
            <TouchableOpacity onPress={this.handlerSubmitChangePhone} 
            style={styles.TouchableOpacityStyle} >
            <Text style={styles.ButtonStyle1}>Save</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.handlerCancel} 
            style={styles.TouchableOpacityStyle} >
            <Text style={styles.ButtonStyle1}>Cancel</Text>
            </TouchableOpacity>
            </View>
        );
        }
        else{
           return(
            <ActivityIndicator size='large' color="#007aff"/>
           ); 
        }
    }

    handlerCancel=()=>{
        this.setState({
            oldPhone:'',
            newPhone:'',
    
            oldPhoneStyle:styles.mobileDiv,
            newPhoneStyle:styles.mobileDiv,
            Introduction:'970',
            IntroductionOld:'970',            
            Errors:{},
        })
    }
    oldPhoneChange=(oldPhone)=>{
        this.setState({oldPhone},()=>{
            if(this.setState.oldPhone=='' || !((/^([9]{1})([7]{1})([0,2]{1})([0-9]{9})*$/).test(this.state.IntroductionOld+this.state.oldPhone))){
                this.setState({oldPhoneStyle:styles.mobileDivError})
                this.setState({
                    Errors:{...this.state.Errors,login:'Old phone number must be of 9 digits '}
                })
            }
            else{
                this.setState({oldPhoneStyle:styles.mobileDiv})
                this.setState({
                    Errors:{...this.state.Errors,login:""}
                })
            }
        })
    }
    handlernewPhoneChange=(newPhone)=>{
        this.setState({newPhone},
            ()=>{
                if(!this.state.newPhone || !((/^([9]{1})([7]{1})([0,2]{1})([0-9]{9})*$/).test(this.state.Introduction+this.state.newPhone))){
                    this.setState({newPhoneStyle:styles.mobileDivError})
                    this.setState({
                        Errors:{...this.state.Errors,login:'New phone number must be of 9 digits '}
                    })
                }
                else{
                    this.setState({newPhoneStyle:styles.mobileDiv})
                    this.setState({
                        Errors:{...this.state.Errors,login:""}
                    })
                }

            })
    }


    handlerSubmitChangePhone =() =>{
        if(!this.state.oldPhone || !this.state.newPhone ){
            this.setState({Errors:{...this.state.Errors,login:strings.PasswordMobileError}})

            if(!this.state.oldPhone || !((/^([9]{1})([7]{1})([0,2]{1})([0-9]{9})*$/).test(this.state.IntroductionOld+this.state.oldPhone))){
                this.setState({oldPhoneStyle:styles.mobileDivError})
            }
            else{
                this.setState({oldPhoneStyle:styles.mobileDiv})
                this.setState({Errors:{...this.state.Errors,login:''}})

            }
            if(!this.state.newPhone || !((/^([9]{1})([7]{1})([0,2]{1})([0-9]{9})*$/).test(this.state.Introduction+this.state.newPhone))){
                this.setState({newPhoneStyle:styles.mobileDivError})
            }
            else{
                this.setState({newPhoneStyle:styles.mobileDiv})
                this.setState({Errors:{...this.state.Errors,login:''}})
            }
        }

        else if(!this.state.oldPhone || !((/^([9]{1})([7]{1})([0,2]{1})([0-9]{9})*$/).test(this.state.IntroductionOld+this.state.oldPhone))){
            this.setState({newPhoneStyle:styles.mobileDivError})
            this.setState({
                Errors:{...this.state.Errors,login:'Old phone number must be of 9 digits '}
            })
        }
        else if(!this.state.newPhone || !((/^([9]{1})([7]{1})([0,2]{1})([0-9]{9})*$/).test(this.state.Introduction+this.state.newPhone))){
            this.setState({newPhoneStyle:styles.mobileDivError})
            this.setState({
                Errors:{...this.state.Errors,login:'New phone number must be of 9 digits '}
            })
        }
        else{
            var data={user_id:this.state.userId , old_phone:this.state.IntroductionOld+this.state.oldPhone , new_phone:this.state.Introduction+this.state.newPhone}
            this.props.updatePhone(data).then(res=>{
                if(this.props.data.data.success){
                    this.setState({isModalVisible:true})
                    this.setState({Errors:{...this.state.Errors,login:''}})
                    setTimeout(function(){
                        this.setState({oldPhone:''})
                        this.setState({newPhone:'' })
                        this.setState({loading:true})
                        this.setState({isModalVisible:false})
                        this.setState({oldPhoneStyle:styles.mobileDiv})
                        this.setState({newPhoneStyle:styles.mobileDiv,        Introduction:'970',
                        IntroductionOld:'970',  })

                        this.setState({Errors:{...this.state.Errors,login:''}})

                        }.bind(this),1400)
                }
                else{
                    this.setState({Errors:{...this.state.Errors,login:'Incorect old phone number'}})
                    if(this.state.oldPhone == this.state.newPhone){
                        this.setState({Errors:{...this.state.Errors,login:'old phone number is the same of new phone number'}})
                    }
                    this.setState({loading:false})
                    setTimeout(function(){
                        this.setState({loading:true})
                        }.bind(this),1000)
                }

            })

           
        }

        
    }

    render(){
        return(
            <KeyboardAvoidingView  style={styles.contianer} >

                <View style={styles.divHome}>
                    <View style={styles.ElementDiv}>
                    <View style={this.state.oldPhoneStyle}>
                        <Picker style={{width:105}}
                                onStartShouldSetResponder={true}
                                selectedValue={this.state.IntroductionOld}
                                onValueChange={(IntroductionOld)=>{this.setState({IntroductionOld})}}>
                            <Picker.Item label='+970'  value="970"></Picker.Item>
                            <Picker.Item label='+972'  value="972"></Picker.Item>
                        </Picker>
                        <TextInput style={[{flex:1}+styles.InputFiled]}
                        placeholder='Old phone Number '
                        returnKeyType="next"
                        onChangeText={this.oldPhoneChange}
                        keyboardType="phone-pad"
                        value={this.state.oldPhone}
                        />
                        </View>
                    </View>
                    <View style={styles.ElementDiv}>
                        <View style={this.state.newPhoneStyle}>
                        <Picker style={{width:105}}
                                onStartShouldSetResponder={true}
                                selectedValue={this.state.Introduction}
                                onValueChange={(Introduction)=>{this.setState({Introduction})}}>
                            <Picker.Item label='+970'  value="970"></Picker.Item>
                            <Picker.Item label='+972'  value="972"></Picker.Item>
                        </Picker>
                        <TextInput style={[{flex:1}+styles.InputFiled]}
                        placeholder='New phone Number '
                        returnKeyType="next"
                        onChangeText={this.handlernewPhoneChange}
                        keyboardType="phone-pad"
                        value={this.state.newPhone}
                        />
                        </View>
                        
                    </View>

                    <View><HanderError>{this.state.Errors.login}</HanderError></View>
                    <View style={styles.ButtonStyle}>
                        {this._renderButton()}
                    </View>
                </View>
                <Modal
                       isVisible={this.state.isModalVisible}
                       onBackButtonPress={()=>{this.setState({isModalVisible:false})}}
                       onBackdropPress={()=>{this.setState({isModalVisible:false})}}
                >
                    <View style={{ flex: 1 , justifyContent:'center' }}>
                        <View style={{  backgroundColor:'white',
                                        padding: 10,
                                        borderRadius:5,
                                        width:'60%',
                                        height:180,
                                        alignSelf:'center',
                                        alignItems:'center'
                                        }}>
                                <Image source={succ} ></Image>
                                <Text style={{fontSize:17 ,marginTop:20,width:'80%',
                                marginLeft:'10%',marginRight:'10%',color:'#000' , alignItems:"center"}}>your password has changed successfly</Text>
                        </View>
                    </View>
                </Modal>

            </KeyboardAvoidingView>
        );
    }
}


const styles=StyleSheet.create({
    contianer:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
    },
    divHome:{
        flex:4,
        justifyContent:'center',
        alignItems:'center',
        width:'80%'
    },
    divEnd:{
        flex:0.2,
        justifyContent:'flex-end',
        alignItems:'center',
    },
    InputFiled:{
        marginLeft:5,
        marginRight:5,
        borderColor:'#c0c0c0',
        borderWidth:1,
        borderRadius:5,
        backgroundColor:'#fff',
        alignSelf:'stretch',
        shadowColor:'#000', 
    },
    InputFiledError:{
        marginLeft:5,
        marginRight:5,
        borderColor:'#d50000',
        borderWidth:1,
        borderRadius:5,
        backgroundColor:'#fff',
        alignSelf:'stretch',
        shadowColor:'#000',
    },
    ElementDiv:{
        marginTop:10,
        marginBottom:10,
        alignSelf:'stretch',   
    },
    TextStyle:{
        alignSelf:'center',
        color:"#808080",
        fontSize:16,
        fontWeight:'600',
        paddingTop:10,
        paddingBottom:10,
    },
    TouchableOpacityStyle:{
        backgroundColor:"#941467",
        width:100 ,
        height:40 ,
        alignItems: 'center',
        justifyContent:'center',
        marginTop:20,
        marginBottom:20,
        borderRadius:5
    },
    ButtonStyle:{
        color:'#FFF',
        fontSize:17,
        fontWeight:'500',
        width:'80%'
    },
    ButtonStyle1:{
        color:'#FFF',
        fontSize:17,
        fontWeight:'500',
    },
    mobileDiv:{
        height:50,
        marginLeft:5,
        marginRight:5,
        borderColor:'#c0c0c0',
        borderWidth:1,
        borderRadius:5,
        backgroundColor:'#fff',
        alignSelf:'stretch',
        shadowColor:'#000', 
        alignItems:'center',
        flexDirection:'row',
    },
    mobileDivError:{
        height:50,
        marginLeft:5,
        marginRight:5,
        borderColor:'#d50000',
        borderWidth:1,
        borderRadius:5,
        backgroundColor:'#fff',
        alignSelf:'stretch',
        shadowColor:'#000', 
        alignItems:'center',
        flexDirection:'row',
    },
})

const maptToProps =dispatch =>{
    return{
        updatePhone:(data)=>dispatch(updateUserPhoneFun(data)),
        
        
    }
}

const mapToState =state =>{
    return{
      data:state.auth
    }
  }


export default connect(mapToState,maptToProps)(changePhoneNum)
