import React,{Component} from 'react';
import { StyleSheet,View,Text,TextInput,Button,KeyboardAvoidingView,Keyboard,Picker
    ,ActivityIndicator,NetInfo , AsyncStorage} from 'react-native'
import HanderError from './inlineError'
import {strings} from './Strings'
import {connect} from 'react-redux'
import {LoginUser} from '../actions/actions'



class Login extends Component{
    constructor(){
        super()
        this.state={
            Mobile:'',
            Password:'',
            Errors:{},
            modalVisible: false,
            language:strings.getLanguage(),
            loading:true,
            backerror:'',
            phoneInputFiled:styles.InputFiled2,
            passwordInputFiled:styles.InputFiled,
            mobileDivFilled:styles.mobileDiv,
            modalVisible: false,
            introductionValue:'970',

        }
    }
  
    static navigationOptions=({navigation})=>{
        return{
            title:strings.titleApp,
            headerStyle:{backgroundColor:'rgb(148,20,103)'},
            headerTitleStyle:{color:'white'},
            headerTintColor:'white',
            headerLeft:null,
            headerRight:(
                <Picker
                onStartShouldSetResponder={true}
                selectedValue={navigation.getParam('language')}
                style={{height: 20, width: 118, color:"#fff",backgroundColor :'transparent'}}
                onValueChange={navigation.getParam('changeLanguage')}>
                <Picker.Item label={strings.EnglishLan} value="en" />
                <Picker.Item label={strings.ArabicLan} value="ar" />
                </Picker>
            )

        }
    }
    componentDidMount(){
        this.props.navigation.setParams({changeLanguage:this.onValueChange})
    }
    Handler_login=()=>{
        this.setState({backerror:''})
        const Errors=this.hander_Validation(this.state)
        this.setState({Errors},()=>{ 
            if(this.state.Errors.login==""){

                NetInfo.isConnected.fetch().done((isConnected)=>{
                    if(isConnected){
                        data={user_phone:this.state.introductionValue+this.state.Mobile,user_password:this.state.Password}
                        this.setState({loading:false})
                        this.props.LoginUser(data)
                        .then(res=>{
                            if(this.props.data.success==false){
                                this.setState({backerror:this.props.data.error})
                                this.setState({mobileDivFilled:styles.mobileDivError})
                                this.setState({passwordInputFiled:styles.InputFiledError});
                            }
                            else{
                                AsyncStorage.setItem('language', strings.getLanguage() )
                                this.props.navigation.navigate('Home')
                             } 
                        })
                        setTimeout(function(){
                            this.setState({loading:true})
                            console.log(this.props.data)
                            }.bind(this),1200)


                    }
                    else{
                        this.setState({loading:false})
                        setTimeout(function(){
                            this.setState({loading:true})
                            alert('no Internet connection')
                        }.bind(this),1000)

                    }

                })
       

            }
        
        })
      
        Keyboard.dismiss()
    }
    componentDidMount(){
        
        this.props.navigation.setParams({changeLanguage:this.onValueChange})
        
    }
    hander_Validation=()=>{
        const errors={}
        if (!this.state.Mobile || !this.state.Password){
            errors.login=strings.PasswordMobileError
            if(!this.state.Mobile){
                this.setState({mobileDivFilled:styles.mobileDivError});
            }
            else{
                this.setState({mobileDivFilled:styles.mobileDiv});
            }
            if(!this.state.Password){
                this.setState({passwordInputFiled:styles.InputFiledError});
            }
            else{
                this.setState({passwordInputFiled:styles.InputFiled}); 
            }
        } 
        else if (!((/^([9]{1})([7]{1})([0,2]{1})([5]{1})([0,2,4,5,6,9]{1})([0-9]{7})*$/).test(this.state.introductionValue+this.state.Mobile)) && this.state.Password){
            errors.login=strings.mobileError
            this.setState({mobileDivFilled:styles.mobileDivError});
            this.setState({passwordInputFiled:styles.InputFiled});

        }
        else if (!(/(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$/).test(this.state.Password) && this.state.Mobile){
            errors.login=strings.passwordError
            this.setState({mobileDivFilled:styles.mobileDiv});
            this.setState({passwordInputFiled:styles.InputFiledError});
        }
        else{
            errors.login=""
            this.setState({passwordInputFiled:styles.InputFiled});
            this.setState({mobileDivFilled:styles.mobileDiv})
        }
      
    
        return errors
    }

    onValueChange=(value, index)=>{
        strings.setLanguage(value)
        this.setState({language:value}) 
        this.props.navigation.setParams({language:strings.getLanguage()})  
    }


    _renderButton(){
        if(this.state.loading){
        return (
            <Button title={strings.LOGIN} onPress={this.Handler_login} color="#941467"></Button>
        );
        }
        else{
           return(
            <ActivityIndicator size='large' color="#007aff"/>
           ); 
        }
    }
    _createAcount=()=>{
        this.props.navigation.navigate('Signup')
    }
    render(){
        
        return(
            <KeyboardAvoidingView  style={styles.contianer} >

           <View style={styles.divHome}>
                <View style={styles.ElementDiv}>
                    <View style={this.state.mobileDivFilled}>

                        <Picker style={{width:105}}
                                onStartShouldSetResponder={true}
                                selectedValue={this.state.introductionValue}
                                onValueChange={(item)=>this.setState({introductionValue:item})}>
                            <Picker.Item label='+970'  value="970"></Picker.Item>
                            <Picker.Item label='+972'  value="972"></Picker.Item>
                        </Picker>

                        <TextInput 
                        style={this.state.phoneInputFiled}
                        placeholder={strings.mobleNumber}
                        returnKeyType="next"
                        onChangeText={(Mobile)=>this.setState({Mobile})}
                        keyboardType="phone-pad"
                        maxLength={9}
                        />

                    </View>
                </View>

                <View style={styles.ElementDiv}>
                <TextInput 
                    secureTextEntry={true}
                    style={this.state.passwordInputFiled}
                    placeholder={strings.password}
                    returnKeyType="next"
                    onChangeText={(Password)=>this.setState({Password})}
                />
                </View>
                <View><HanderError>{this.state.Errors.login}</HanderError></View>
                <View><HanderError>{this.state.backerror}</HanderError></View>
                <View style={styles.ButtonStyle}>
                        {this._renderButton()}
                </View>

                <View>
                <Text style={styles.TextStyle}>{strings.ForgetPassword}</Text>
                </View>

            </View>

            <View style={styles.divEnd}>
            
            <Text style={styles.TextStyle} onPress={this._createAcount}>{strings.CreatedAccount}</Text>
            </View>
      
     </KeyboardAvoidingView>
        )
        
    }
}
const styles=StyleSheet.create({
    model:{
        width:100,
        width:100,
        backgroundColor:"red"
    },
    contianer:{
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },
   
    divHome:{
        flex:4,
        justifyContent:'center',
        alignItems:'center',

    },
    divEnd:{
        flex:0.5,
        justifyContent:'flex-end',
        alignItems:'center',
    },
    InputFiled:{
        marginLeft:5,
        marginRight:5,
        borderColor:'#c0c0c0',
        borderWidth:1,
        borderRadius:5,
        backgroundColor:'#fff',
        alignSelf:'stretch',
        shadowColor:'#000',        
    },

    InputFiled2:{
        marginLeft:5,
        marginRight:5,
        borderColor:'#c0c0c0',
        backgroundColor:'#fff',
        alignSelf:'stretch',
        shadowColor:'#000', 
        flex:1 , 
    },


    InputFiledError:{
        marginLeft:5,
        marginRight:5,
        borderColor:'#d50000',
        borderWidth:1,
        borderRadius:5,
        backgroundColor:'#fff',
        alignSelf:'stretch',
        shadowColor:'#000',
    },
    ElementDiv:{
        marginTop:10,
        marginBottom:10,
        alignSelf:'stretch',  

    },
    mobileDiv:{
        height:50,
        marginLeft:5,
        marginRight:5,
        borderColor:'#c0c0c0',
        borderWidth:1,
        borderRadius:5,
        backgroundColor:'#fff',
        alignSelf:'stretch',
        shadowColor:'#000', 
        alignItems:'center',
        flexDirection:'row',
    },
    mobileDivError:{
        height:50,
        marginLeft:5,
        marginRight:5,
        borderColor:'#d50000',
        borderWidth:1,
        borderRadius:5,
        backgroundColor:'#fff',
        alignSelf:'stretch',
        shadowColor:'#000', 
        alignItems:'center',
        flexDirection:'row',
    },

    TextStyle:{
        alignSelf:'center',
        color:"#808080",
        fontSize:16,
        fontWeight:'600',
        paddingTop:10,
        paddingBottom:10,
    },
    ButtonStyle:{
        width:300,
    },


})
const mapToProps = dispatch =>{
    return{
      LoginUser:(data)=>dispatch(LoginUser(data))
    }

  }

const mapToState =state =>{
    return{
      data:state.auth
    }
  }


export default connect(mapToState,mapToProps)(Login); 

