import axios from 'axios'
const link='http://ec2-18-234-52-106.compute-1.amazonaws.com:9091'

export default{
    login:{
        login:data=>
        axios.post(link+'/api/auth',data).then(res=>res)
    },
    signUp:{
        signUp:data=>
        axios.post(link+'/api/signUp',data).then(res=>res)
    },
    creatBook:{
        creatBook:data=>
        axios.post(link+'/api/book',data).then(res=>res)
    },
    getAllBooks:{
        getAllBooks:data=>
        axios.post(link+'/api/AllUserBook',data).then(res=>res)
    },
    EditBook:{
        EditBook:data=>
        axios.put(link+'/api/book' ,data).then(res=>res)
    },
    DeleteBook:{
        DeleteBook:data=>
        axios.post(link+'/api/DeleteBook', data).then(res=>res)
    },
    getBookCheque:{
        getBookCheque:data=>
        axios.post(link+'/api/AllBookcheques',data).then(res=>res)
    },
    addCheque:{
        addCheque:data=>
        axios.post(link+'/api/cheque',data).then(res=>res)
    },
    editCheque:{
        editCheque:data=>
        axios.put(link+'/api/cheque',data).then(res=>res)
    },
    deleteCheque:{
        deleteCheque:data=>
        axios.delete(link+'/api/cheque',{params:{cheque_id:data}})
    },
    AddChequeRecive:{
        AddChequeRecive:data=>
        axios.post(link+'/api/recivedCheque',data).then(res=>res)
    },
    ChequeReciveData:{
        ChequeReciveData:data=>
        axios.post(link+'/api/recivedChequesUser',data).then(res=>res)
    },
    DeleteReciveCheque:{
        DeleteReciveCheque:data=>
            axios.delete(link+'/api/recivedCheque',{params:{id:data}}).then(res=>res)
        
    },
    UpdateReciveCheques:{
        UpdateReciveCheques:data=>
            axios.put(link+'/api/recivedCheque',data).then(res=>res)
    },
    
    getAllCreateCheques:{
        getAllCreateChequesA:data=>
            axios.get(link+'/api/alluserCheques',{params:{user_id:data}}).then(res=>res)
        
    },

    
    UpdateReciveChequesStatus:{
        UpdateReciveChequesStatus:data=>
            axios.put(link+'/api/recivedChequeStatus',data).then(res=>res)
         
    },
    UpdateChequeStatus:{
        UpdateChequeStatus:data=>
        axios.put(link+'/api/chequeupdate',data).then(res=>res)
    },
    UpdateBookAvailableSize:{
        UpdateBookAvailableSize:data=>
        axios.put(link+'/api/bookAvailableSize' ,data).then(res=>res)
    
    },
    UpdateUserPassword:{
        UpdateUserPassword:data=>
        axios.put(link+'/api/changePassword',data).then(res=>res)
    },
    UpdateUserPhone:{
        UpdateUserPhone:data=>
        axios.put(link+'/api/changePhone',data).then(res=>res)
    },

}