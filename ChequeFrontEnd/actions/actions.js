import {LOGIN,LOGIN_FAILED,SIGNUP,SIGNUP_FAILED,creatBook,getAllBooks,
    EditBook,DeleteBook,GetBookCheques ,ADDCHEQUE,ChequeRecive,chequeReciveDate
    ,getCreateCheques,UPDATEPASSWORD,UPDATEPHONE} from './type'

import QS from 'qs'
import api from './api'
import {AsyncStorage} from 'react-native'
    
export const LoginUser = (data) =>{
    return async (dispatch)=>{
    dispatch({type:LOGIN})
    await api.login.login(QS.stringify(data))
    .then(resp=>handleUser(dispatch,resp.data))
    .catch(err=>console.log(err))
    }
}
const handleUser =(dispatch,data)=>{
    console.log(data)
if(data.success){
    signIn_attempt(dispatch,data)
}
else{
    signIN_failed(dispatch,data)
}

}
const signIn_attempt=(dispatch,data)=>{
AsyncStorage.multiSet([['userId',''+data.user_id] ,['token',data.token]])
    .then(()=>
    {
        dispatch({
            type:LOGIN,
            data
        });
    })
}
const signIN_failed=(dispatch,data)=>{
dispatch({
    type:LOGIN_FAILED,
    data
});
}
export const CreateUser = (data) =>{
return async (dispatch)=>{
    dispatch({type:SIGNUP})
    await api.signUp.signUp(QS.stringify(data))
    .then(resp=>handleCreateUser(dispatch,resp.data))
    .catch(err=>console.log(err))
}
}
const handleCreateUser =(dispatch,data)=>{
if(data.success==true){
    createUser_attempt(dispatch,data)
}
else{
    createUser_failed(dispatch,data)
}

}
const createUser_attempt = (dispatch,data)=>{
AsyncStorage.multiSet([['userId',''+data.user_id] ,['token',data.token]])
    .then(()=>
{
    dispatch({
        type:SIGNUP,
        data
    });

})

}
const createUser_failed =(dispatch,data)=>{
dispatch({
    type:SIGNUP_FAILED,
    data
});
}

export const updateUserPasswordFunction =(data)=>{
    return async (dispatch)=>{
        dispatch({type:UPDATEPASSWORD})
        await api.UpdateUserPassword.UpdateUserPassword(QS.stringify(data))
        .then(resp=>updateUserPasswordData(dispatch,resp.data))
    }
}
const updateUserPasswordData=(dispatch,data)=>{
    dispatch({
        type:UPDATEPASSWORD,
        data
    })
}
export const updateUserPhoneFun=(data)=>{
    return async (dispatch)=>{
        dispatch({type:UPDATEPHONE})
        await api.UpdateUserPhone.UpdateUserPhone(QS.stringify(data))
        .then(resp=>updateUserPhoneData(dispatch,resp.data))
    }
}

const updateUserPhoneData=(dispatch , data)=>{
    dispatch({
        type:UPDATEPHONE,
        data
    })
}


export const CreateNewBook =(data) =>{
    return async (dispatch)=>{
        dispatch({type:creatBook})
        await api.creatBook.creatBook(QS.stringify(data))
        .then(resp=>creatBookData(dispatch,resp.data))
    }
}

const creatBookData=(dispatch,data)=>{
    dispatch({
        type:creatBook,
        data
    })
}

export const GetAllBooksFun=(data)=>{
    return async (dispatch)=>{
        dispatch({type:getAllBooks})
        await api.getAllBooks.getAllBooks(QS.stringify(data))
        .then(resp=>getAllBooksData(dispatch,resp.data))
    }
}

const getAllBooksData=(dispatch,data)=>{
    dispatch({
        type:getAllBooks,
        data
    })
}

export const EditBookFun=(data)=>{
    return async(dispatch)=>{
        dispatch({type:EditBook})
        await api.EditBook.EditBook(QS.stringify(data))
        .then(resp=>EditBookData(dispatch,resp.data))
    }
}

EditBookData=(dispatch,data)=>{
    dispatch({
        type:EditBook,
        data
    })
}

export const DeleteBookFun=(data)=>{
    return async(dispatch)=>{
        dispatch({type:DeleteBook})
        await api.DeleteBook.DeleteBook(QS.stringify(data))
        .then(resp=>DeleteBookData(dispatch,resp.data))
    }
}

DeleteBookData=(dispatch,data)=>{
    dispatch({
        type:DeleteBook,
    })
}
export const AddChequeReciveFun=(data)=>{
    return async(dispatch)=>{
        dispatch({type:ChequeRecive})
        await api.AddChequeRecive.AddChequeRecive(QS.stringify(data))
        .then(resp=>addChequeReciveData(dispatch,resp.data))
    }
}

const addChequeReciveData=(dispatch,data)=>{
    dispatch({
        type:ChequeRecive,
        data
    })
}
export const  ChequeReciveDatafun=(data)=>{
    
    return async (dispatch)=>{
        dispatch({type:chequeReciveDate})
        await api.ChequeReciveData.ChequeReciveData(QS.stringify(data))
        .then(resp=>{ChequeReciveDataInfo(dispatch,resp.data)
        })
    }
}

const ChequeReciveDataInfo=(dispatch,data)=>{
    dispatch({
        type:chequeReciveDate,
        data
    })
}

export const GetAllBooksChequesFun=(data)=>{
    return async(dispatch)=>{
        dispatch({type:GetBookCheques})
        await api.getBookCheque.getBookCheque(QS.stringify(data))
        .then(resp=>getAllChequesData(dispatch,resp.data))
    }
}

getAllChequesData=(dispatch,data)=>{
    dispatch({
        type:GetBookCheques,
        data
    })
}

export const addChequeFun=(data)=>{
    return async (dispatch)=>{
        dispatch({type:ADDCHEQUE})
        await api.addCheque.addCheque(QS.stringify(data))
        .then(resp=>addChequeData(dispatch,resp.data))
    }
}

const addChequeData=(dispatch,data)=>{
    dispatch({
        type:ADDCHEQUE,
        data
    })
}

export const UpdateReciveChequestatusfun=(data)=>{
    return async(dispatch)=>{
        await api.UpdateReciveChequesStatus.UpdateReciveChequesStatus(QS.stringify(data))
    }
}

export const editChequeFun=(data)=>{
    return async(dispatch)=>{
        await api.editCheque.editCheque(QS.stringify(data))
    }
}

export const deletechequeFun=(data)=>{
    return async(dispatch)=>{
        await api.deleteCheque.deleteCheque(data)
    }
}
export const  DeleteReciveDatafun=(data)=>{
    return async (dispatch)=>{
        dispatch({type:chequeReciveDate})
        await api.DeleteReciveCheque.DeleteReciveCheque(data)
       
    }
}
export const  UpdateReciveChequefun=(data)=>{
    
    return async (dispatch)=>{
        await api.UpdateReciveCheques.UpdateReciveCheques(QS.stringify(data))
       
    }
}

export const GetALlChequesCreate=(UserId)=>{
    return async(dispatch)=>{
        dispatch({type:getCreateCheques})
        await api.getAllCreateCheques.getAllCreateChequesA(UserId).
        then(res=>getAllcreateChequesData(dispatch,res.data))
    }
}

getAllcreateChequesData=(dispatch,data)=>{
    dispatch({
        type:getCreateCheques,
        data
    })
}
export const updateChequeStatusFun=(data)=>{
    return async (dispatch)=>{
        await api.UpdateChequeStatus.UpdateChequeStatus(QS.stringify(data))
    }
}

export const UpdateBookAvailableSizeFun=(data)=>{
    return async (dispatch)=>{
        await api.UpdateBookAvailableSize.UpdateBookAvailableSize(QS.stringify(data))
    }
}




