
import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import Route from './Route/routeNavigation'
import {applyMiddleware,createStore} from 'redux'
import {Provider} from 'react-redux';
import reducer from './Reducers'
import thunk from 'redux-thunk'

class App extends Component {
  render() {
    return (
      
      <Provider store={createStore(reducer ,{},applyMiddleware(thunk))}>
          <View style={styles.container}>
            
              <Route screenProps={'ar'}></Route>
          </View> 
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

});
export default App