const INITIAL_SATATE={ user:null ,loading:false ,error:''}

import {LOGIN ,SIGNUP ,LOGIN_FAILED ,SIGNUP_FAILED,creatBook
    ,getAllBooks,EditBook,GetBookCheques, ADDCHEQUE,chequeReciveDate,ChequeRecive
    ,getCreateCheques,UPDATEPASSWORD ,UPDATEPHONE} from '../actions/type'


export default (state=INITIAL_SATATE ,action) =>{
    switch(action.type){
        case LOGIN:
            return {success:true,data:action.data}
        
        case LOGIN_FAILED:
            return {success:false ,error:action.data.error}
        
        case SIGNUP:
            return {success:true, data:action.data}
        
        case SIGNUP_FAILED:
            return {success:false , error:action.data.error}
        
        case creatBook:
            return {success:true,data:action.data}
        
        case getAllBooks:
            return {data:action.data}

        case chequeReciveDate:
            return {data:action.data}
        
        case ChequeRecive:
            return {data:action.data}
        
        case EditBook:
            return {success:true}
        
        case GetBookCheques:
            return {data:action.data}
        
        case ADDCHEQUE:
            return {data:action.data}
        case getCreateCheques:
            return {data:action.data}
        
        case UPDATEPASSWORD:
            return {data:action.data}
        
        case UPDATEPHONE:
            return {data:action.data}
        default:
            return state
    }
}