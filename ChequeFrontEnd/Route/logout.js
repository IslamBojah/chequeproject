import React,{PureComponent} from 'react'
import {View,AsyncStorage,Alert} from  "react-native"


const logout = (props) =>{

   

        Alert.alert(
            'LogOutMessage',
            'Are you sure,you want to Logout app?',
            [
              {
                  
                text: 'Cancel',
                onPress: ()=>props.navigation.goBack(),
                style: 'cancel',
              },
              {text: 'OK', onPress: () => {
                AsyncStorage.clear()
                props.navigation.navigate("Auth")
              }
            
            },
            ],
            {cancelable: false},
          );

          return(
              <View></View>
          )
   
    
}

/*class logout extends PureComponent{

    componentWillMount(){
       
        Alert.alert(
            'LogOutMessage',
            'Are you sure,you want to Logout app?',
            [
              {
                  
                text: 'Cancel',
                onPress: ()=>this.props.navigation.goBack(),
                style: 'cancel',
              },
              {text: 'OK', onPress: () => {
                AsyncStorage.clear()
                this.props.navigation.navigate("Auth")
              }
            
            },
            ],
            {cancelable: false},
          );
        }
      
    

    render(){
        return(
            <View></View>
        )
    }

}
*/

export default logout

