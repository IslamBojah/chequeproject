import {createBottomTabNavigator,createDrawerNavigator,DrawerItems} from 'react-navigation'
import ReceiveCheque from '../component/ReceiveCheque'
import CreateCheque from '../component/CreateCheque'
import Calendar from '../component/calendar'
import React from 'react'
import {SafeAreaView,View,Image,ScrollView} from 'react-native'


const TabNavigator=createBottomTabNavigator({
Calendar:{
    screen:Calendar,
    navigationOptions: {
        activeTintColor: '#e91e63',
      },
  
},
ReceiveCheque:ReceiveCheque,
CreateCheque:CreateCheque,


},{
    activeTintColor: '#e91e63'
})

const CustomDrawerComponent=(props)=>(
    <SafeAreaView style={{flex:1}}>
      <View style={{height:150,backgroundColor:'white',alignItems:'center',justifyContent:'center'}}>
      </View>
      <ScrollView>
        <DrawerItems {...props}></DrawerItems>
      </ScrollView>
    </SafeAreaView>
  )

  const MyDrawerNavigator = createDrawerNavigator({
    Home: {
      screen: TabNavigator,
    },
    Notifications: {
      screen: ReceiveCheque,
    },
  },{
      contentComponent:CustomDrawerComponent
  });
  
  export default MyDrawerNavigator