import {createStackNavigator,createDrawerNavigator,
  createBottomTabNavigator,DrawerItems,createSwitchNavigator} from 'react-navigation'
import Splash from '../component/splash'
import Login from '../component/login'
import Signup from '../component/signup'
import Home  from '../component/Home'
import ReceiveCheque from '../component/ReceiveCheque'
import CreateCheque from '../component/CreateCheque'
import React from 'react';
import {SafeAreaView,View,Image,ScrollView,Dimensions,Text,
  TouchableOpacity,Alert,AsyncStorage} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import BookCheques from '../component/BookCheques'
import {strings} from '../component/Strings'
import ChangePassword from '../component/ChangePassword'
import changePhoneNum from '../component/changePhoneNum'
import logout from './logout'



const language=strings.getLanguage()

AsyncStorage.getItem('language').then((lan)=>{
    language= lan
    alert(language)

  
})


const CustomDrawerComponent=(props)=>(
  <SafeAreaView style={{flex:1}}>
      <View style={{backgroundColor:'white',alignItems:'center',justifyContent:'center',flexDirection:'row',flex:.5}}>
      <Text style={{fontWeight:'bold',fontSize:15,color:'black'}} >{strings.titleApp}</Text>
      </View>
      <ScrollView>
        <DrawerItems {...props}></DrawerItems>
      </ScrollView>
    </SafeAreaView>
)


const stacNavChangePassword=createStackNavigator({
  ChangePassword:{
    screen:ChangePassword,
    navigationOptions: ({navigation}) => ({
      title:'Change Password',
      headerTitleStyle:{color:'white'},
      headerStyle:{backgroundColor:'rgb(148,20,103)'},
      headerLeft: (
        <Icon
          style={{ paddingLeft: 10,color:'white' }}
          onPress={() => navigation.openDrawer()}
          name="bars"
          size={25}
          
        />
      )
      
       })
  }
})

const stacNavChangePhoneNum=createStackNavigator({
  ChangePassword:{
    screen:changePhoneNum,
    navigationOptions: ({navigation}) => ({
      title:'Change Phone Number',
      headerTitleStyle:{color:'white'},
      headerStyle:{backgroundColor:'rgb(148,20,103)'},
      headerLeft: (
        <Icon
          style={{ paddingLeft: 10,color:'white' }}
          onPress={() => navigation.openDrawer()}
          name="bars"
          size={25}
        />
      )
      
       })
  }
})
const stackNav=createStackNavigator({
  CreateCheque:{
    screen:CreateCheque,
    navigationOptions: () => ({
      title:strings.Calendar,
      header:null,
       })
},
BookCheques:{
  screen:BookCheques,
  navigationOptions: () => ({
    header:null,
     })
},
})


const TabNavigator=createBottomTabNavigator({
  Calendar:{
    screen:Home,
    navigationOptions: {
    title: strings.Calendar,
    tabBarIcon: ({ tintColor }) =><Icon name="calendar" size={20} />
  }
  },
  CreateCheque:{
    screen:stackNav,
    navigationOptions: {
      title:strings.Checkbooks,
      tabBarIcon: ({ tintColor }) =><Icon name="bank" size={20} />
    }
  },
  ReceiveCheque:{screen:ReceiveCheque
    ,
    navigationOptions: {
      title: strings.ReceiveCheque,
      tabBarIcon: ({ tintColor }) =><Icon name="pencil" size={20} />
    }
  }
    },{
      //initialRouteName:Calendar,
      //order:['Calendar','CreateCheque','ReceiveCheque'],
      tabBarOptions: {
        activeTintColor: 'rgb(148,20,103)',
        labelStyle: {
          fontSize: 12,
          fontStyle: "normal",
          textAlign: "center",           
        }
      },
  
      })

      const StabBottom=createStackNavigator({
        DashboardTabNavigator: {
          screen:TabNavigator,
          navigationOptions: ({ navigation }) => {
            
            const { routeName } = navigation.state.routes[navigation.state.index];
            return {
              headerTitle: routeName,
              headerTitleStyle:{color:'white'},
              headerStyle:{backgroundColor:'rgb(148,20,103)'},
              headerLeft: (
                <Icon
                  style={{ paddingLeft: 10,color:'white' }}
                  onPress={() => navigation.openDrawer()}
                  name="bars"
                  size={25}
                  
                />
              ),             
            };
          }
          }
    })

      const MyDrawerNavigator = createDrawerNavigator({
        MainHome: {
          screen: TabNavigator,
          title:strings.Home,
          navigationOptions: {
      //      navigation.setParams({title:strings.Home})
      //      return{
            title:strings.Home,
            drawerIcon: ({ tintColor }) => (
              <Icon
                name="home"
                resizeMode="contain"
               size={20}
              />
            
          )
       // }
          }
        
        },
        ChangePassword:{
          screen:ChangePassword,
          navigationOptions: {
            title:strings.ChangePassword,
            drawerIcon: ({ tintColor }) => (
              <Icon
                name="lock"
                resizeMode="contain"
               size={20}
              />
            )
          }
        },
      changePhoneNumber:{
        screen:changePhoneNum,
        navigationOptions: {
          title:strings.PhoneNumber,
          drawerIcon: ({ tintColor }) => (
            <Icon
              name="mobile"
              resizeMode="contain"
             size={20}
            />
          )
        }
    
      },
      logout:{
        screen:logout,
        navigationOptions: {
          title:strings.logout,
          drawerIcon: ({ tintColor }) => (
            <Icon
              name="sign-out"
              resizeMode="contain"
             size={20}
            />
          )
        }
      }
        
      },
    
        {
        drawerWidth:(Dimensions.get('window').width)/2,
        contentComponent:CustomDrawerComponent,
        contentOptions: {
          activeTintColor :'white',
           inactiveTintColor :'#000',
          activeBackgroundColor :'rgb(148,20,103)',
          inactiveBackgroundColor :'#fff',

        },
        drawerPosition:language=='ar'?'right':'left'

    });


const AppStack=createStackNavigator({
  Home:{screen:MyDrawerNavigator,
  navigationOptions: ({ navigation }) => {
    const { routeName } = navigation.state.routes[navigation.state.index];
   // alert(navigation.state.params.title)
    return {
      title:routeName ,
     // headerTitle:navigation.state.params.title,
      headerTitleStyle:{color:'white'},
      headerStyle:{backgroundColor:'rgb(148,20,103)'},
      headerLeft: (
        <Icon
          style={{ paddingLeft: 10,color:'white' }}
          onPress={() => navigation.openDrawer()}
          name="bars"
          size={25}
          
        />
      ),
    
      
      
    };
  }
}})
const AuthStack=createStackNavigator({Login:Login , Signup:Signup})

export default createSwitchNavigator({
  AuthLoading:Splash,
  App:AppStack,
  Auth:AuthStack
},
{
  initialRouteName:'AuthLoading'
},

)













